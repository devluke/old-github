# pyOS
pyOS is an OS in Python. There are applications, tools, etc. pyOS is based off of iOS, the iPad, iPhone, and iPod touch OS.
## Applications
There are a few applications in pyOS. If you need help with any of them, read about it below.
### Calculator
The calculator is a tool that you use to add, subtract, multiply, and divide. Once you type calculator, you will see your cursor will go to a new line and there will be no text. Simpily type in a number (-5- * 3) and press enter. Then, type in an operation (+ = add, - = subtract, * = multiply, / = divide). Then type in a last number (5 * -3-). Then, you should get the answer to your problem.
### Settings
The settings app has a bunch of settings. Below is a list of the following settings.
#### About
When you type "about", it says pyOS [current version of pyOS] and system information.
## Other
There are also some commands that you can use in pyOS.
### Lock
Type "lock" to lock your pyOS. When it locks, it will shut down and ask for your password to boot again.
