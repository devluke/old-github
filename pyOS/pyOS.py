from __future__ import print_function, division

import sys
import math
import os

print("Booting...")

password = raw_input("Welcome to pyOS. Please enter a password and press enter or return.")

name = raw_input("What is your name?")

print("Setting up pyOS...")

print("pyOS v1.0 - type 'help' for help")
 
command1 = raw_input()

if command1 == "help":
	help = raw_input("What do you need help with? (commands, other)")

if help == "commands":
	print("Here is a list of commands: calculator, settings, lock")

if help == "other":
	print("For more information, visit the GitHub page at .")

if command1 == "calculator":
	num1 = int(raw_input())
	operation = raw_input()
	num2 = int(raw_input())
	if operation == "+":
		add = num1 + num2
	if operation == "-":
		subtract = num1 - num2
	if operation == "*":
		multiply = num1 * num2
	if operation == "/":
		divide = num1 / num2
	if operation == "+":
		print(add)
	if operation == "-":
		print(subtract)
	if operation == "*":
		print(multiply)
	if operation == "/":
		print(divide)

if command1 == "settings":
	setting = raw_input()
	if setting == "about":
		print("pyOS v1.0",os.uname())

if command1 == "lock":
	print("Locked.")
	tries = 0
	while tries < 3:
		unlock = raw_input("Password: ")
		if unlock == password:
			print("Welcome back, {}.".format(name))
			break
		else:
			tries = tries + 1
	else: 
		print("Alert: You have tried unlocking pyOS too many times.")
		print("Shutting down...")

