# Obsidian
Obsidian is a small, work-in-progress programming language built in Java.

## API
Obsidian has an easy-to-use API and a more advanced, customizable API directly running from the main language class. The default, easy-to-use API is recommended for anybody.

### Default API
The default Obsidian API is all in the `ObsidianRun.java` class and is built off of the direct API. The methods are as follows.

#### runLine(String line, String file, String lineNumber)
This is to run a simple line of code. This method is not recommended as there are other methods that would most likely work better for your purpose.

##### String line
The line of code

##### String file
The name of the file

##### String lineNumber
The number of the current line

#### runLine(String line)
This is to run a simple line of code. This just uses the above method but uses the parameters `line, "run", "1"`.

##### String line
The line of code

#### runCode(List<String> code)
This runs each line of code in a String list. This does NOT use the below function.

##### List<String> code
The list of code

#### runCode(List<String> code, String file)
This runs each line of code in a String list. This allows you to specify the file name. This method is not recommended as there are other methods that would most likely work better for your purpose.

##### List<String> code
The list of code

##### String file
The name of the file

#### runFile(File file)
This reads code from a file and runs it as a list of code.

##### File file
The file with the code

#### resultHandle(List<String> result, String lineNumber, String file, String line)
This handles the results of the `parseAndRun` function in the direct API and formats it in a Python-like error style.

##### List<String> result
The non-formatted result of `parseAndRun` in a list

##### String lineNumber
The number of the line the result is from

##### String file
The name of the file the result is from

##### String line
The line of code the result is from

### Direct API
The API directly inside of the main language class (`Obsidian.java`). It consists of a single method and is NOT recommended for ANY user.

#### parseAndRun(String code)
Parse and run a string of code

##### String code
The string of code

##### Result
The result is a string list.

If the code run with no errors, it will return `["0"]`.

Otherwise, it will return `["1", "[{Error Name}]{Error Description}"` where `{Error Name}` is the type of error and `{Error Description}` is the description of the error.