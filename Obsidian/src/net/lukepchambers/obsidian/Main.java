package net.lukepchambers.obsidian;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static Obsidian obInstance = new Obsidian();
    private static ObsidianRun runInstance = new ObsidianRun();

    private static List<String> commandHelp = new ArrayList<String>();

    public Main() {
        commandHelp.add("Usage: obsidian [option | file]");
        commandHelp.add("Where options are:");
        commandHelp.add("--help - Prints this help menu");
        commandHelp.add("--version - Prints the current Obsidian version");
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            Scanner scanner = new Scanner(System.in);
            while (true) {
                System.out.print(">>> ");
                String codeLine = scanner.nextLine();
                try {
                    runInstance.runLine(codeLine);
                } catch (Exception e) {
                    if (obInstance.debug) {
                        e.printStackTrace();
                    }
                    System.out.println("CRITICAL: Unhandled Java exception.");
                    System.out.println("Please open an issue on TheLukeGuy/Obsidian on GitHub.");
                }
            }
        } else if (args.length == 1) {
            if (args[0].equalsIgnoreCase("--help")) {
                for (String i : commandHelp) {
                    System.out.println(i);
                }
            } else if (args[0].equalsIgnoreCase("--version")) {
                System.out.println("Obsidian version " + obInstance.version);
            } else {
                if (args[0].startsWith("-")) {
                    System.out.println("Unknown option: " + args[0]);
                    System.exit(1);
                } else {
                    File argFile = new File(args[0]);
                    if (!argFile.exists()) {
                        System.out.println("No such file or directory.");
                        System.exit(1);
                    }
                    if (!args[0].contains(".ob")) {
                        System.out.println("Note: File name does not have the extension \".ob\".");
                        System.out.println("It is recommended to change this as it may become unsupported in the future.\n");
                    }
                    runInstance.runFile(argFile);
                }
            }
        } else {
            System.out.println("Please specify 1 option only");
            System.exit(1);
        }
    }
}
