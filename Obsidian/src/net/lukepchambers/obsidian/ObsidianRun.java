package net.lukepchambers.obsidian;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;

public class ObsidianRun {
    private Obsidian obInstance = new Obsidian();

    public void runLine(String line, String file, String lineNumber) {
        resultHandle(obInstance.parseAndRun(line), lineNumber, file, line);
    }

    public void runLine(String line) {
        runLine(line, "run", "1");
    }

    public void runCode(List<String> code) {
        for (String i : code) {
            runLine(i, "run", String.valueOf(code.indexOf(i)));
        }
    }

    public void runCode(List<String> code, String file) {
        for (String i : code) {
            runLine(i, file, String.valueOf(code.indexOf(i)));
        }
    }

    public void runFile(File file) {
        try {
            runCode(Files.readAllLines(file.toPath(), Charset.forName("utf-8")), file.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void resultHandle(List<String> result, String lineNumber, String file, String line) {
        if (result.get(0).equals("0")) {
            return;
        } else if (result.get(0).equals("1")) {
            String errorType = result.get(1).substring(result.get(1).indexOf("[")+1,result.get(1).indexOf("]"));
            String errorMessage = result.get(1).substring(result.get(1).indexOf("]")+1);

            System.out.println("Error on line " + lineNumber + " of " + file);
            System.out.println("    " + line);
            System.out.println("    ^");
            System.out.println(errorType + ": " + errorMessage);
            return;
        } else {
            System.out.println("CRITICAL: Error handling result.");
            System.out.println("Please open an issue on TheLukeGuy/Obsidian on GitHub.");
        }
    }
}
