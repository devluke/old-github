package net.lukepchambers.obsidian;

import java.util.*;

public class Obsidian {
    String version = "0.0.1-ALPHA";
    boolean debug = true;
    private Map<String, List<String>> storedData = new HashMap<String, List<String>>();

    public List<String> parseAndRun(String code) {
        // Split code by words
        String[] codeList = code.split(" ");

        if (codeList[0].equalsIgnoreCase("var")) {
            String newVarName;

            try {
                // Set a variable!
                newVarName = codeList[1];

                // Check if correct declaration
                if (!codeList[2].equals("=")) {
                    return newStringList("1", "[SyntaxError]Incorrect declaration of variable");
                }

                // Actually create the variable
                storedData.put(newVarName, newStringList("var", String.join(" ", listToList(codeList).subList(3, codeList.length))));

            } catch (Exception e) {
                return newStringList("1", "[SyntaxError]Incorrect declaration of variable");
            }

            if (debug) {
                System.out.println(String.format("[DEBUG] %s = %s", newVarName, String.join(" ", listToList(codeList).subList(3, codeList.length))));
            }

            return newStringList("0");
        }

        if (storedData.containsKey(codeList[0])) {
            // Run a function or print a variable
            String dataName = codeList[0];
            List<String> data = storedData.get(dataName);

            // Detect data type
            if (data.get(0).equalsIgnoreCase("var")) {
                // Variable
                if (codeList.length == 1) {
                    // Print variable
                    System.out.println(data.get(1));

                } else if (codeList.length >= 3) {
                    // Change value of variable
                    if (codeList[1].equals("=")) {
                        storedData.put(codeList[0], newStringList("var", String.join(" ", listToList(codeList).subList(2, codeList.length))));

                        if (debug) {
                            System.out.println(String.format("[DEBUG] %s = %s", codeList[0], String.join(" ", listToList(codeList).subList(2, codeList.length))));
                        }

                    } else {
                        return newStringList("1", "[SyntaxError]Unknown syntax");
                    }

                } else {
                    return newStringList("1", "[SyntaxError]Unknown syntax");
                }
            }

            return newStringList("0");
        }

        if (codeList.length >= 2) {
            if (codeList[1].equals("=")) {
                // Tried changing variable value but variable doesn't exist
                return newStringList("1", "[DataError]Variable hasn't been declared");
            }
        }

        if (!(code != null && !code.isEmpty())) {
            // Line is empty, ignore syntax errors
            return newStringList("0");
        }

        return newStringList("1", "[SyntaxError]Unknown syntax");
    }

    private List<String> newStringList(String ... items) {
        List<String> tempList = new ArrayList<String>();

        for (String i : items) {
            tempList.add(i);
        }

        return tempList;
    }

    private List<String> listToList(String[] toConvert) {
        return new ArrayList<String>(Arrays.asList(toConvert));
    }
}