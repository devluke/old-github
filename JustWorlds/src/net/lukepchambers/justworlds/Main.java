package net.lukepchambers.justworlds;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.event.world.WorldInitEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.cloutteam.samjakob.gui.ItemBuilder;
import com.cloutteam.samjakob.gui.buttons.GUIButton;
import com.cloutteam.samjakob.gui.types.PaginatedGUI;
import com.gamerking195.dev.autoupdaterapi.UpdateLocale;
import com.gamerking195.dev.autoupdaterapi.Updater;
import com.onarandombox.MultiverseCore.WorldProperties;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class Main extends JavaPlugin implements Listener {
	private Server server = Bukkit.getServer();
	private ConsoleCommandSender console = server.getConsoleSender();
	private PluginManager pm = server.getPluginManager();

	private String prefix = "§bJustWorlds §3» §7";
	private String error = "§c§lError §3» §7";

	String version = this.getDescription().getVersion();
	private double configVersion = 10;

	private List<String> commands = new ArrayList<>();
	private List<File> unmigrated = new ArrayList<>();

	private boolean pluginEnabled = true;
	private boolean updateAvailable;

	@Override
	public void onLoad() {
		console.sendMessage(prefix + "Searching for worlds...");
		File[] directories = new File(this.getDataFolder() + File.separator + ".." + File.separator + "..").listFiles(File::isDirectory);
		List<File> worlds = new ArrayList<>();

		try {
			for (File i : directories) {
				File levelDat = new File(i + File.separator + "level.dat");

				if (levelDat.exists()) {
					worlds.add(i);
				}
			}

		} catch (Exception e) {
			// Fixes NullPointerException
		}

		console.sendMessage(prefix + "Checking worlds for JustWorlds format...");

		for (File i : worlds) {
			File jwFile = new File(i + File.separator + "world.jw");

			if (jwFile.exists()) {

				try {
					commands.add(new String(Files.readAllBytes(jwFile.toPath())).trim());
				} catch (IOException e) {
					e.printStackTrace();
				}

			} else {
				unmigrated.add(i);
			}
		}

		if (!(unmigrated.size() == 0)) console.sendMessage(prefix + unmigrated.size() + " worlds still need to be migrated to the JustWorlds format.");

		console.sendMessage(prefix + "Worlds will be loaded after the plugin is enabled.");
	}

	@Override
	public void onEnable() {
		pluginEnabled = false;

		pm.registerEvents(this, this);

		PaginatedGUI.prepare(this);

		console.sendMessage(prefix + "Unloading default worlds...");

		for (World i : server.getWorlds()) {
			for (Chunk chunk : i.getLoadedChunks()) {
				chunk.unload(false);
			}

			server.getWorlds().remove(i);
			server.unloadWorld(i, false);
		}

		console.sendMessage(prefix + "Loading worlds...");

		for (String i : commands) {
			server.dispatchCommand(console, i);
		}

		for (World i : server.getWorlds()) {
			File jwFile = new File(i.getWorldFolder() + File.separator + "world.pw");

			if (jwFile.exists()) {
				File spawnFile = new File(i.getWorldFolder() + File.separator + "spawn.pw");

				String fileContents = null;

				try {
					fileContents = new String(Files.readAllBytes(spawnFile.toPath())).trim();
				} catch (IOException e) {
					e.printStackTrace();
				}

				String[] spawnLocation = fileContents.split(",");
				List<Integer> intSpawnLocation = new ArrayList<>();

				for(String s : spawnLocation) {
					intSpawnLocation.add(Integer.valueOf(s));
				}

				i.setSpawnLocation(intSpawnLocation.get(0), intSpawnLocation.get(1), intSpawnLocation.get(2));
			}
		}

		saveDefaultConfig();

		if (getConfig().getDouble("version") < configVersion) {
			console.sendMessage(prefix + "Updating config.yml...");

			YamlConfiguration configYml = YamlConfiguration.loadConfiguration(new File(this.getDataFolder() + File.separator + "config.yml"));

			File configFile = new File(this.getDataFolder(), "config.yml");
			configFile.delete();
			saveDefaultConfig();
			reloadConfig();

			for (String i : configYml.getKeys(true)) {
				try {
					if (!i.equals("version")) {
						getConfig().get(i);
						getConfig().set(i, configYml.get(i));
						saveConfig();
					}
				} catch (Exception e) {
					// That key doesn't exist in the new config.yml file
				}
			}

			console.sendMessage(prefix + "The config.yml file was updated.");
		}

		prefix = translateColorCodes(getConfig().getString("messages.prefix"));
		error = translateColorCodes(getConfig().getString("messages.error"));
		
		Metrics metrics = new Metrics(this);
		metrics.addCustomChart(new Metrics.DrilldownPie("serverAddress", () -> {
			Map<String, Map<String, Integer>> map = new HashMap<>();
			Map<String, Integer> entry = new HashMap<>();
			
			if (getConfig().getBoolean("enables.address")) entry.put(server.getIp(), 1);
			else entry.put("Hidden", 1);
			
			map.put("Port " + server.getPort(), entry);
			
			return map;
		}));

		console.sendMessage(prefix + "Checking for updates...");

		try {
			updateAvailable = checkForUpdates();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (updateAvailable) {
			console.sendMessage(prefix + "An update is available. Use /jwupdate to update.");
		} else {
			console.sendMessage(prefix + "The plugin is up-to-date.");
		}

		pluginEnabled = true;
		console.sendMessage(prefix + "Plugin enabled.");
	}

	@Override
	public void onDisable() {
		console.sendMessage(prefix + "Plugin disabled.");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		boolean playerSender = sender instanceof Player;
		Player player = playerSender ? server.getPlayer(sender.getName()) : null;

		if (cmd.getName().equalsIgnoreCase("justworlds")) {
			if (args.length == 0) {
				if (playerSender) {
					PaginatedGUI menu = new PaginatedGUI("JustWorlds Info");

					GUIButton button1 = new GUIButton(ItemBuilder.start(Material.DIAMOND_BLOCK).name("§6§lJustWorlds v" + version).build());
					button1.setListener(e -> {
						e.setCancelled(true);
					});
					menu.setButton(21, button1);

					GUIButton button2 = new GUIButton(ItemBuilder.start(Material.NAME_TAG).name("§a§lBy TheLukeGuy").build());
					button2.setListener(e -> {
						e.setCancelled(true);
					});
					menu.setButton(22, button2);

					GUIButton button3 = new GUIButton(ItemBuilder.start(Material.BOOK).name("§c§lHelp: /jwhelp").lore("§6§lLeft-click to run /jwhelp.").build());
					button3.setListener(e -> {
						e.setCancelled(true);
						player.closeInventory();
						server.dispatchCommand(sender, "jwhelp");
					});
					menu.setButton(23, button3);

					player.openInventory(menu.getInventory());
				} else {
					sender.sendMessage(prefix + "This server is running JustWorlds version " + version + " by TheLukeGuy.");
					sender.sendMessage(prefix + "For a list of commands, use /jwhelp.");
				}

				return true;
			}

			List<String> newArgs = Arrays.asList(args).subList(1, args.length);

			server.dispatchCommand(sender, "jw" + args[0] + " " + String.join(" ", newArgs));

			return true;
		}


		if (cmd.getName().equalsIgnoreCase("jwhelp")) {
			int arg = 1;

			if (args.length == 0) {
				arg = 1;
			} else if (args.length == 1) {
				try {
					arg = Integer.parseInt(args[0]);
				} catch (Exception e) {
					sender.sendMessage(error + "Invalid page.");

					return true;
				}
			} else {
				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			List<String> helpMenu = new ArrayList<>();
			int lastPage = (int) ((Math.round(this.getDescription().getCommands().size() / 8.0) * 8) / 8);

			if (arg > lastPage) {
				sender.sendMessage(error + "Invalid page.");

				return true;
			}

			Map<String, Map<String, Object>> commands = this.getDescription().getCommands();
			List<String> easyCommands = new ArrayList<>(commands.keySet());

			if (playerSender) {
				PaginatedGUI menu = new PaginatedGUI("JustWorlds Help");

				int menuItem = 0;

				for (String i : easyCommands) {
					Command command = this.getCommand(i);

					boolean hasPerm = command.getPermission() == null ? true : (sender.hasPermission(command.getPermission()) ? true : false);

					GUIButton button = new GUIButton(ItemBuilder.start(hasPerm ? Material.EMERALD_BLOCK : Material.REDSTONE_BLOCK)
							.name((hasPerm ? "§a" : "§c") + "§l/" + command.getName())
							.lore(Arrays.asList(
									command.getUsage().replaceAll("<command>", command.getName()),
									"",
									(hasPerm ? "§a" : "§c") + "§lPermission §3» §7" + (command.getPermission() == null ? "none" : command.getPermission()),
									(hasPerm ? "§a§lPermission §3» §7You have permission to use this." : "§c§lPermission §3» §7You do not have permission to use this."),
									"",
									"§6§lLeft-click to run /" + command.getName() + "."
									))
							.build()
							);

					button.setListener(e -> {
						e.setCancelled(true);
						player.closeInventory();
						server.dispatchCommand(sender, command.getName());
					});

					menu.setButton(menuItem, button);

					menuItem++;
				}

				player.openInventory(menu.getInventory());

			} else {
				int firstIndex = (8 * (arg - 1));
				int secondIndex = arg * 8;

				List<String> helpList = null;

				if (arg == lastPage) {
					helpList = easyCommands.subList(firstIndex, easyCommands.size());
				} else {
					helpList = easyCommands.subList(firstIndex, secondIndex);
				}

				for (String command : helpList) {
					helpMenu.add(command);
				}

				sender.sendMessage(prefix + "List of JustWorlds commands:");

				for (String i : helpMenu) {
					Command command = this.getCommand(i);

					TextComponent message = new TextComponent("/" + command.getName());
					TextComponent separator = new TextComponent(" - ");
					TextComponent description = new TextComponent(command.getDescription());

					message.setColor(command.getPermission() == null ? ChatColor.GREEN : (sender.hasPermission(command.getPermission()) ? ChatColor.GREEN : ChatColor.RED));
					separator.setColor(ChatColor.GRAY);
					description.setColor(ChatColor.GRAY);

					String permission = "\n\n" + ((command.getPermission() == null ? "§a" : (sender.hasPermission(command.getPermission()) ? "§a" : "§c")) + "§lPermission §3» §7" + (command.getPermission() == null ? "none" : command.getPermission()));
					String hasPermission = command.getPermission() == null ? "§a§lPermission §3» §7You have permission to use this." : (sender.hasPermission(command.getPermission()) ? "§a§lPermission §3» §7You have permission to use this." : "§c§lPermission §3» §7You do not have permission to use this.");

					message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(command.getUsage().replaceAll("<command>", command.getName()) + permission + "\n" + hasPermission).create()));

					message.addExtra(separator);
					message.addExtra(description);

					sender.spigot().sendMessage(message);
				}

				sender.sendMessage(prefix + "You are viewing page " + arg + "/" + lastPage + ".");
			}

			return true;
		}


		if (cmd.getName().equalsIgnoreCase("jwcreate")) {
			{
				List<String> argsList = Arrays.asList(args);
				List<String> newArgs = new ArrayList<>();

				for (String i : argsList) {
					newArgs.add(i.replaceAll("`colon`", ":"));
				}

				args = newArgs.toArray(new String[0]);
			}

			String lagWarning = prefix + "A world is being created, lag may occur.";

			if (args.length == 0) {
				// No arguments, invalid

				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			File jwFile = new File(this.getDataFolder() + File.separator + ".." + File.separator + ".." + File.separator + args[0] + File.separator + "world.jw");
			File worldFolder = new File(this.getDataFolder() + File.separator + ".." + File.separator + ".." + File.separator + args[0]);
			File levelDat = new File(this.getDataFolder() + File.separator + ".." + File.separator + ".." + File.separator + args[0] + File.separator + "level.dat");

			if (jwFile.exists() && !(unmigrated.contains(worldFolder)) && pluginEnabled) {
				sender.sendMessage(error + "That world already exists.");

				return true;
			}

			if (worldFolder.exists() && !(levelDat.exists()) && pluginEnabled) {
				sender.sendMessage(error + "A folder with that name already exists.");

				return true;
			}

			if (args.length == 1) {
				// One argument, create a normal world with a name

				WorldCreator world = new WorldCreator(args[0]);

				if (pluginEnabled) broadcastPlayers(lagWarning);
				world.createWorld();
			}

			if (args.length == 2) {
				// Two arguments, create a custom-environment world with a name

				WorldCreator world = new WorldCreator(args[0]);

				if (args[1].equalsIgnoreCase("normal")) {
					world.environment(Environment.NORMAL);

				} else if (args[1].equalsIgnoreCase("nether")) {
					world.environment(Environment.NETHER);

				} else if (args[1].equalsIgnoreCase("the_end")) {
					world.environment(Environment.THE_END);

				} else {
					sender.sendMessage(error + "Invalid environment.");

					return false;
				}

				if (pluginEnabled) broadcastPlayers(lagWarning);
				world.createWorld();
			}

			if (args.length == 3) {
				// Three arguments, invalid

				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			if (args.length == 4) {
				// Four arguments, create a custom-environment world with a name and a custom seed, generator, or type

				WorldCreator world = new WorldCreator(args[0]);

				if (args[1].equalsIgnoreCase("normal")) {
					world.environment(Environment.NORMAL);

				} else if (args[1].equalsIgnoreCase("nether")) {
					world.environment(Environment.NETHER);

				} else if (args[1].equalsIgnoreCase("the_end")) {
					world.environment(Environment.THE_END);

				} else {
					sender.sendMessage(error + "Invalid environment.");

					return false;
				}

				if (args[2].equalsIgnoreCase("-s")) {
					try {
						world.seed(Long.parseLong(args[3]));
					} catch (NumberFormatException e) {
						world.seed(args[3].hashCode());
					}
				}

				if (args[2].equalsIgnoreCase("-g")) {
					world.generator(args[3]);
				}

				if (args[2].equalsIgnoreCase("-t")) {
					if (args[3].equalsIgnoreCase("normal")) {
						world.type(WorldType.NORMAL);
					} else if (args[3].equalsIgnoreCase("flat")) {
						world.type(WorldType.FLAT);
					} else if (args[3].equalsIgnoreCase("amplified")) {
						world.type(WorldType.AMPLIFIED);
					} else if (args[3].equalsIgnoreCase("large_biomes")) {
						world.type(WorldType.LARGE_BIOMES);
					} else if (args[3].equalsIgnoreCase("empty")) {
						world.type(WorldType.FLAT);
						world.generatorSettings("2;0;1;");
					} else {
						sender.sendMessage(error + "Invalid world type.");

						return false;
					}
				}

				if (pluginEnabled) broadcastPlayers(lagWarning);
				world.createWorld();
			}

			if (args.length == 5) {
				// Five arguments, invalid

				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			if (args.length == 7) {
				// Seven arguments, invalid

				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			if (args.length == 6 || args.length == 8) {
				// Six or seven arguments, create a custom-environment world with a name and a two of: custom seed, generator, or type

				WorldCreator world = new WorldCreator(args[0]);

				if (args[1].equalsIgnoreCase("normal")) {
					world.environment(Environment.NORMAL);

				} else if (args[1].equalsIgnoreCase("nether")) {
					world.environment(Environment.NETHER);

				} else if (args[1].equalsIgnoreCase("the_end")) {
					world.environment(Environment.THE_END);

				} else {
					sender.sendMessage(error + "Invalid environment.");

					return false;
				}

				if (args[2].equalsIgnoreCase("-s")) {
					try {
						world.seed(Long.parseLong(args[3]));
					} catch (NumberFormatException e) {
						world.seed(args[3].hashCode());
					}
				}

				if (args[2].equalsIgnoreCase("-g")) {
					world.generator(args[3]);
				}

				if (args[2].equalsIgnoreCase("-t")) {
					if (args[3].equalsIgnoreCase("normal")) {
						world.type(WorldType.NORMAL);
					} else if (args[3].equalsIgnoreCase("flat")) {
						world.type(WorldType.FLAT);
					} else if (args[3].equalsIgnoreCase("amplified")) {
						world.type(WorldType.AMPLIFIED);
					} else if (args[3].equalsIgnoreCase("large_biomes")) {
						world.type(WorldType.LARGE_BIOMES);
					} else if (args[3].equalsIgnoreCase("empty")) {
						world.type(WorldType.FLAT);
						world.generatorSettings("2;0;1;");
					} else {
						sender.sendMessage(error + "Invalid world type.");

						return false;
					}
				}

				if (args[4].equalsIgnoreCase("-s")) {
					try {
						world.seed(Long.parseLong(args[5]));
					} catch (NumberFormatException e) {
						world.seed(args[5].hashCode());
					}
				}

				if (args[4].equalsIgnoreCase("-g")) {
					world.generator(args[5]);
				}

				if (args[4].equalsIgnoreCase("-t")) {
					if (args[5].equalsIgnoreCase("normal")) {
						world.type(WorldType.NORMAL);
					} else if (args[5].equalsIgnoreCase("flat")) {
						world.type(WorldType.FLAT);
					} else if (args[5].equalsIgnoreCase("amplified")) {
						world.type(WorldType.AMPLIFIED);
					} else if (args[5].equalsIgnoreCase("large_biomes")) {
						world.type(WorldType.LARGE_BIOMES);
					} else if (args[5].equalsIgnoreCase("empty")) {
						world.type(WorldType.FLAT);
						world.generatorSettings("2;0;1;");
					} else {
						sender.sendMessage(error + "Invalid world type.");

						return false;
					}
				}

				try {
					if (args[6].equalsIgnoreCase("-s")) {
						try {
							world.seed(Long.parseLong(args[7]));
						} catch (NumberFormatException e) {
							world.seed(args[7].hashCode());
						}
					}

					if (args[6].equalsIgnoreCase("-g")) {
						world.generator(args[7]);
					}

					if (args[6].equalsIgnoreCase("-t")) {
						if (args[7].equalsIgnoreCase("normal")) {
							world.type(WorldType.NORMAL);
						} else if (args[7].equalsIgnoreCase("flat")) {
							world.type(WorldType.FLAT);
						} else if (args[7].equalsIgnoreCase("amplified")) {
							world.type(WorldType.AMPLIFIED);
						} else if (args[7].equalsIgnoreCase("large_biomes")) {
							world.type(WorldType.LARGE_BIOMES);
						} else if (args[7].equalsIgnoreCase("empty")) {
							world.type(WorldType.FLAT);
							world.generatorSettings("2;0;1;");
						} else {
							sender.sendMessage(error + "Invalid world type.");

							return false;
						}
					}
				} catch (Exception e) {
					// There aren't 7-8 arguments.
				}

				if (pluginEnabled) broadcastPlayers(lagWarning);
				world.createWorld();
			}

			if (args.length >= 8) {
				// Eight or more arguments, invalid

				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			String command = "jwcreate " + String.join(" ", args).replaceAll(":", "`colon`");
			List<String> commandList = new ArrayList<String>(Arrays.asList(command.split(":")));

			try {
				Files.write(jwFile.toPath(), commandList, Charset.forName("UTF-8"));
			} catch (IOException e) {
				e.printStackTrace();
			}

			World world = server.getWorld(args[0]);

			File spawnFile = new File(world.getWorldFolder() + File.separator + "spawn.jw");
			List<String> spawnList = new ArrayList<String>(Arrays.asList("none"));

			try {
				Files.write(spawnFile.toPath(), spawnList, Charset.forName("UTF-8"));
			} catch (IOException e) {
				e.printStackTrace();
			}

			if (unmigrated.contains(world.getWorldFolder())) {
				unmigrated.remove(world.getWorldFolder());
			}

			if (pluginEnabled || sender instanceof Player) {
				sender.sendMessage(prefix + args[0] + " has been created.");
			}

			return true;
		}


		if (cmd.getName().equalsIgnoreCase("jwvisit")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(error + "This command can only be run by players.");

				return true;
			}

			if (args.length != 1) {
				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			World world = server.getWorld(args[0]);

			try {
				player.teleport(world.getSpawnLocation());
			} catch (Exception e) {
				sender.sendMessage(error + "Invalid world.");

				return true;
			}

			sender.sendMessage(prefix + "Teleported to " + world.getName() + ".");

			return true;
		}


		if (cmd.getName().equalsIgnoreCase("jwsetspawn")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(error + "This command can only be run by players.");

				return true;
			}

			if (args.length > 0) {
				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			World world = null;

			world = player.getWorld();

			world.setSpawnLocation(player.getLocation());

			File spawnFile = new File(world.getWorldFolder() + File.separator + "spawn.jw");
			Location spawnLocation = world.getSpawnLocation();
			String coords = spawnLocation.getX() + "," + spawnLocation.getY() + "," + spawnLocation.getZ();
			List<String> coordsList = new ArrayList<String>(Arrays.asList(coords.split(":")));

			try {
				Files.write(spawnFile.toPath(), coordsList, Charset.forName("UTF-8"));
			} catch (IOException e) {
				e.printStackTrace();
			}

			sender.sendMessage(prefix + "Set spawn for " + world.getName() + ".");

			return true;
		}


		if (cmd.getName().equalsIgnoreCase("jwspawn")) {
			if (args.length == 0) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(error + "This command can only be run by players.");

					return true;
				}

				player.teleport(player.getWorld().getSpawnLocation());

				sender.sendMessage(prefix + "Teleported to the world spawn.");
			}

			if (args.length == 1) {
				if (!sender.hasPermission("justworlds.spawn.others")) {
					sender.sendMessage(permission("justworlds.spawn.others"));

					return true;
				}

				Player tpPlayer = server.getPlayer(args[0]);

				try {
					tpPlayer.teleport(tpPlayer.getWorld().getSpawnLocation());
				} catch (Exception e) {
					sender.sendMessage(error + "Unknown user.");

					return true;
				}

				sender.sendMessage(prefix + "Teleported " + tpPlayer.getName() + " to the world spawn.");
				tpPlayer.sendMessage(prefix + sender.getName() + " teleported you to the world spawn.");
			}

			if (args.length >= 2) {
				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			return true;
		}

		if (cmd.getName().equalsIgnoreCase("jwworld")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(error + "This command can only be run by players.");

				return true;
			}

			if (!(args.length == 0)) {
				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			World world = server.getPlayer(sender.getName()).getWorld();

			if (playerSender) {
				PaginatedGUI menu = new PaginatedGUI(world.getName());

				GUIButton button1 = new GUIButton(ItemBuilder.start(Material.DIAMOND_BLOCK).name("§6§l" + world.getName()).build());
				button1.setListener(e -> {
					e.setCancelled(true);
				});
				menu.setButton(21, button1);

				GUIButton button2 = new GUIButton(ItemBuilder.start(Material.BOAT).name("§a§lInfo for " + world.getName()).lore("§6§lLeft-click to get info for " + world.getName() + ".").build());
				button2.setListener(e -> {
					e.setCancelled(true);
					player.closeInventory();
					server.dispatchCommand(sender, "jwinfo " + world.getName());
				});
				menu.setButton(22, button2);

				GUIButton button3 = new GUIButton(ItemBuilder.start(Material.SKULL_ITEM).name("§e§lPlayers in " + world.getName()).lore("§6§lLeft-click to get the players in " + world.getName() + ".").build());
				button3.setListener(e -> {
					e.setCancelled(true);
					player.closeInventory();
					server.dispatchCommand(sender, "jwwho " + world.getName());
				});
				menu.setButton(23, button3);

				player.openInventory(menu.getInventory());
			} else {
				sender.sendMessage(prefix + "You are currently in " + world.getName() + ".");
			}

			return true;
		}

		if (cmd.getName().equalsIgnoreCase("jwlist")) {
			if (!(args.length == 0)) {
				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			if (playerSender) {
				PaginatedGUI menu = new PaginatedGUI("Worlds");

				int menuItem = 0;

				for (File i : unmigrated) {
					List<String> worlds = new ArrayList<>();

					for (World world : server.getWorlds()) {
						worlds.add(world.getName());
					}

					if (!(worlds.contains(i.getName()))) {
						GUIButton button = new GUIButton(ItemBuilder.start(Material.GOLD_BLOCK)
								.name("§e§l" + i.getName())
								.lore(Arrays.asList("§eThis world is in the server folder but isn't loaded.", "§eHave an op use /jwcreate to load it.", "", "§6§lLeft-click for world info."))
								.build()
								);

						button.setListener(e -> {
							e.setCancelled(true);
							player.closeInventory();
							server.dispatchCommand(sender, "jwinfo " + i.getName());
						});

						menu.setButton(menuItem, button);

						menuItem++;
					}
				}

				for (World i : server.getWorlds()) {
					boolean isMigrated = false;
					File jwFile = new File(i.getWorldFolder() + File.separator + "world.jw");

					if (jwFile.exists()) isMigrated = true;

					final boolean finalMigrated = isMigrated;

					GUIButton button = new GUIButton(ItemBuilder.start(isMigrated ? Material.EMERALD_BLOCK : Material.REDSTONE_BLOCK)
							.name((isMigrated ? "§a" : "§c") + "§l" + i.getName())
							.lore(isMigrated ? Arrays.asList("§aThis world has been migrated to the JustWorlds format.", "", "§6§lLeft-click for world info.") : Arrays.asList("§cThis world has not been migrated to the JustWorlds format.", "§cHave an op join this world for more info."))
							.build()
							);

					button.setListener(e -> {
						e.setCancelled(true);

						if (finalMigrated) {
							player.closeInventory();
							server.dispatchCommand(sender, "jwinfo " + i.getName());
						}
					});

					menu.setButton(menuItem, button);

					menuItem++;
				}

				player.openInventory(menu.getInventory());
			} else {
				sender.sendMessage(prefix + "List of worlds:");

				for (File i : unmigrated) {
					TextComponent message = new TextComponent("§3" + i.getName() + " §7- ");

					TextComponent unloaded = new TextComponent("Unloaded");

					unloaded.setColor(ChatColor.YELLOW);
					unloaded.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("This world is in the server folder but isn't loaded. Have an op use /jwcreate to load it.").create()));

					message.addExtra(unloaded);

					List<String> worlds = new ArrayList<>();

					for (World world : server.getWorlds()) {
						worlds.add(world.getName());
					}

					if (!(worlds.contains(i.getName()))) {
						sender.spigot().sendMessage(message);
					}
				}

				for (World i : server.getWorlds()) {
					boolean isMigrated = false;
					File jwFile = new File(i.getWorldFolder() + File.separator + "world.jw");

					if (jwFile.exists()) {
						isMigrated = true;
					}

					TextComponent message = new TextComponent("§3" + i.getName() + " §7- ");

					TextComponent migrated = new TextComponent("Migrated");
					TextComponent unmigrated = new TextComponent("Unmigrated");

					migrated.setColor(ChatColor.GREEN);
					unmigrated.setColor(ChatColor.RED);

					migrated.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("This world has been migrated to the JustWorlds format.").create()));
					unmigrated.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("This world has not been migrated to the JustWorlds format. Have an op join this world for more info.").create()));

					if (isMigrated) {
						message.addExtra(migrated);
					} else {
						message.addExtra(unmigrated);
					}

					sender.spigot().sendMessage(message);
				}
			}

			return true;
		}

		if (cmd.getName().equalsIgnoreCase("jwinfo")) {
			World world = null;

			if (args.length == 0) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(error + "This command can only be run by players.");

					return true;
				}

				world = server.getPlayer(sender.getName()).getWorld();
			}

			if (args.length == 1) {
				world = server.getWorld(args[0]);
			}

			if (args.length > 1) {
				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			File worldFile;

			try {
				worldFile = new File(world.getWorldFolder() + File.separator + "world.jw");
			} catch (Exception e) {
				sender.sendMessage(error + "Invalid world.");

				return true;
			}

			if (!worldFile.exists()) {
				sender.sendMessage(error + "Please have an op migrate this world to the JustWorlds format to get info for this world. If you are an op, log out and back into the server for instructions.");

				return true;
			}

			if (playerSender) {
				PaginatedGUI menu = new PaginatedGUI("Info for " + world.getName());
				final World finalWorld = world;

				GUIButton button1 = new GUIButton(ItemBuilder.start(Material.LEAVES)
						.name("§2§lEnvironment")
						.lore(Arrays.asList("§7" + world.getEnvironment().toString().toLowerCase()))
						.build()
						);
				button1.setListener(e -> {
					e.setCancelled(true);
				});
				menu.setButton(20, button1);

				GUIButton button2 = new GUIButton(ItemBuilder.start(Material.SEEDS)
						.name("§a§lSeed")
						.lore(Arrays.asList("§7" + world.getSeed()))
						.build()
						);
				button2.setListener(e -> {
					e.setCancelled(true);
				});
				menu.setButton(21, button2);

				GUIButton button3 = new GUIButton(ItemBuilder.start(Material.IRON_BLOCK)
						.name("§8§lGenerator")
						.lore(Arrays.asList("§7" + (world.getGenerator() == null ? "none" : world.getGenerator().toString())))
						.build()
						);
				button3.setListener(e -> {
					e.setCancelled(true);
				});
				menu.setButton(23, button3);

				GUIButton button4 = new GUIButton(ItemBuilder.start(Material.GRASS)
						.name("§6§lType")
						.lore("§7" + world.getWorldType().toString().toLowerCase())
						.build()
						);
				button4.setListener(e -> {
					e.setCancelled(true);
				});
				menu.setButton(24, button4);

				GUIButton button5 = new GUIButton(ItemBuilder.start(Material.BOAT)
						.name("§a§lTeleport to " + world.getName())
						.lore("§6§lLeft-click to teleport to " + world.getName() + ".")
						.build()
						);
				button5.setListener(e -> {
					e.setCancelled(true);
					player.closeInventory();
					server.dispatchCommand(sender, "jwvisit " + finalWorld.getName());
				});
				menu.setButton(4, button5);

				player.openInventory(menu.getInventory());
			} else {
				sender.sendMessage(prefix + "Info for " + world.getName() + ":");

				sender.sendMessage("§3Environment §7- " + world.getEnvironment().toString().toLowerCase());
				sender.sendMessage("§3Seed §7- " + world.getSeed());
				sender.sendMessage("§3Generator §7- " + (world.getGenerator() == null ? "none" : world.getGenerator().toString()));
				sender.sendMessage("§3Type §7- " + world.getWorldType().toString().toLowerCase());
			}

			return true;
		}

		if (cmd.getName().equalsIgnoreCase("jwdelete")) {
			if (args.length == 0 || args.length > 2) {
				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			World world = server.getWorld(args[0]);

			if (world == null) {
				sender.sendMessage(error + "Invalid world.");

				return true;
			}

			if (args.length == 1 || (args.length == 2 && !args[1].equalsIgnoreCase("confirm"))) {
				if (playerSender) {
					PaginatedGUI menu = new PaginatedGUI("Confirm");

					GUIButton button1 = new GUIButton(ItemBuilder.start(Material.NAME_TAG)
							.name("§6§lPlease Confirm")
							.lore(Arrays.asList("§cThis will delete the world " + world.getName() + " and ALL it's files.", "§cThere is NO way to recover this world after."))
							.build()
							);
					button1.setListener(e -> {
						e.setCancelled(true);
					});
					menu.setButton(4, button1);

					GUIButton button2 = new GUIButton(ItemBuilder.start(Material.LIME_SHULKER_BOX)
							.name("§a§lYes")
							.lore(Arrays.asList("§aMake sure you're aware of what this will do!"))
							.build()
							);
					button2.setListener(e -> {
						e.setCancelled(true);
						player.closeInventory();
						server.dispatchCommand(sender, "jwdelete " + world.getName() + " confirm");
					});
					menu.setButton(20, button2);

					GUIButton button3 = new GUIButton(ItemBuilder.start(Material.RED_SHULKER_BOX)
							.name("§c§lNo")
							.lore(Arrays.asList("§cThe action will be completely cancelled."))
							.build()
							);
					button3.setListener(e -> {
						e.setCancelled(true);
						player.closeInventory();
					});
					menu.setButton(24, button3);

					player.openInventory(menu.getInventory());
					return true;
				} else {
					sender.sendMessage(prefix + "This will unmigrate the world " + args[0] + " from the JustWorlds format. If you are ABSOLUTELY sure this is what you want to do, add \"confirm\" to the end of the command.");
					sender.sendMessage(prefix + "/jwunmigrate " + args[0] + " confirm");

					return true;
				}
			}

			try {
				for (Player worldPlayer : world.getPlayers()) {
					worldPlayer.kickPlayer("§cThe world you are currently in is being deleted.\n\nYou've been kicked to prevent problems to the server.\n\nYou may rejoin the server whenever you want.");
				}
			} catch (Exception e) {
				sender.sendMessage(error + "Invalid world.");

				return true;
			}

			server.unloadWorld(world, true);

			try {
				FileUtils.deleteDirectory(world.getWorldFolder());
			} catch (IOException e) {
				e.printStackTrace();
			}

			sender.sendMessage(prefix + world.getName() + " has been deleted.");

			return true;
		}

		if (cmd.getName().equalsIgnoreCase("jwwho")) {
			if (args.length > 1) {
				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			if (args.length == 0) {
				if (playerSender) {
					PaginatedGUI menu = new PaginatedGUI("All Players");

					int menuItem = 0;

					for (World i : server.getWorlds()) {
						if (!(i.getPlayers().size() == 0)) {
							List<String> lore = new ArrayList<>();

							for (Player worldPlayer : i.getPlayers().subList(0, i.getPlayers().size())) {
								lore.add("§a" + worldPlayer.getName());
							}

							GUIButton button = new GUIButton(ItemBuilder.start(Material.GRASS)
									.name("§6§l" + i.getName())
									.lore(lore)
									.build()
									);

							button.setListener(e -> {
								e.setCancelled(true);
							});

							menu.setButton(menuItem, button);

							menuItem++;
						} else {
							GUIButton button = new GUIButton(ItemBuilder.start(Material.DIRT).name("§6§l" + i.getName()).lore(Arrays.asList("§cNo players!")).build());

							button.setListener(e -> {
								e.setCancelled(true);
							});

							menu.setButton(menuItem, button);

							menuItem++;
						}
					}

					player.openInventory(menu.getInventory());
				} else {
					sender.sendMessage(prefix + "List of players:");

					for (World i : server.getWorlds()) {
						if (!(i.getPlayers().size() == 0)) {
							String playerList = "§7";

							for (Player worldPlayer : i.getPlayers().subList(0, i.getPlayers().size() - 1)) {
								playerList = playerList + worldPlayer.getName() + "§8, §7";
							}

							playerList = playerList + i.getPlayers().get(i.getPlayers().size() - 1).getName();

							sender.sendMessage("§3" + i.getName() + " §7- " + playerList);
						}
					}
				}
			}

			if (args.length == 1) {
				World world = server.getWorld(args[0]);

				if (world == null) {
					sender.sendMessage(error + "Invalid world.");

					return true;
				}

				if (playerSender) {
					PaginatedGUI menu = new PaginatedGUI("Players in " + world.getName());

					int menuItem = 0;

					if (!(world.getPlayers().size() == 0)) {
						List<String> lore = new ArrayList<>();

						for (Player worldPlayer : world.getPlayers().subList(0, world.getPlayers().size())) {
							lore.add("§a" + worldPlayer.getName());
						}

						GUIButton button = new GUIButton(ItemBuilder.start(Material.GRASS)
								.name("§6§l" + world.getName())
								.lore(lore)
								.build()
								);

						button.setListener(e -> {
							e.setCancelled(true);
						});

						menu.setButton(menuItem, button);

						menuItem++;
					} else {
						GUIButton button = new GUIButton(ItemBuilder.start(Material.DIRT).name("§6§l" + world.getName()).lore(Arrays.asList("§cNo players!")).build());

						button.setListener(e -> {
							e.setCancelled(true);
						});

						menu.setButton(menuItem, button);

						menuItem++;
					}

					player.openInventory(menu.getInventory());
				} else {
					if (!(world.getPlayers().size() == 0)) {
						String playerList = "§7";

						sender.sendMessage(prefix + "List of players:");

						for (Player worldPlayer : world.getPlayers().subList(0, world.getPlayers().size() - 1)) {
							playerList = playerList + worldPlayer.getName() + "§8, §7";
						}

						playerList = playerList + world.getPlayers().get(world.getPlayers().size() - 1).getName();

						sender.sendMessage("§3" + world.getName() + " §7- " + playerList);
					}
				}
			}

			return true;
		}

		if (cmd.getName().equalsIgnoreCase("jwunload")) {
			if (!(args.length == 1)) {
				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			if (server.getWorld(args[0]) == null) {
				sender.sendMessage(error + "Invalid world.");

				return true;
			}

			World world = server.getWorld(args[0]);

			for (Player worldPlayer : world.getPlayers()) {
				worldPlayer.kickPlayer("§eThe world you are currently in is being unloaded.\n\nYou've been kicked to prevent problems to the server.\n\nYou may rejoin the server whenever you want.");
			}

			unmigrated.add(world.getWorldFolder());
			server.unloadWorld(world, true);

			sender.sendMessage(prefix + world.getName() + " has been unloaded.");

			return true;
		}

		if (cmd.getName().equalsIgnoreCase("jwupdate")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(error + "This command can only be run by players.");

				return true;
			}

			if (args.length > 1) {
				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			if (args.length == 1 && !args[0].equalsIgnoreCase("force")) {
				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			if (!(args.length == 1)) {
				sender.sendMessage(prefix + "Checking for updates...");
				try {
					updateAvailable = checkForUpdates();
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (!updateAvailable) {
					sender.sendMessage(error + "The plugin is already up-to-date.");

					return true;
				}
			}

			if (!(pm.isPluginEnabled("AutoUpdaterAPI"))) {
				sender.sendMessage(error + "You need the AutoUpdaterAPI plugin to use this.");

				return true;
			}

			sender.sendMessage(prefix + "Updating JustWorlds...");

			UpdateLocale locale = new UpdateLocale();

			locale.setFileName("JustWorlds");
			locale.setPluginName("JustWorlds");

			new Updater((Player) sender, this, 53792, locale, false, true).update();

			return true;
		}

		if (cmd.getName().equalsIgnoreCase("jwunmigrate")) {
			if (args.length == 0 || args.length > 2) {
				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			World world = server.getWorld(args[0]);

			if (world == null) {
				sender.sendMessage(error + "Invalid world.");

				return true;
			}

			if (args.length == 1 || (args.length == 2 && !args[1].equalsIgnoreCase("confirm"))) {
				if (playerSender) {
					PaginatedGUI menu = new PaginatedGUI("Confirm");

					GUIButton button1 = new GUIButton(ItemBuilder.start(Material.NAME_TAG)
							.name("§6§lPlease Confirm")
							.lore(Arrays.asList("§cThis will unmigrate the world " + world.getName() + ".", "§cThis world will NOT be loaded when the server starts."))
							.build()
							);
					button1.setListener(e -> {
						e.setCancelled(true);
					});
					menu.setButton(4, button1);

					GUIButton button2 = new GUIButton(ItemBuilder.start(Material.LIME_SHULKER_BOX)
							.name("§a§lYes")
							.lore(Arrays.asList("§aMake sure you're aware of what this will do!"))
							.build()
							);
					button2.setListener(e -> {
						e.setCancelled(true);
						player.closeInventory();
						server.dispatchCommand(sender, "jwunmigrate " + world.getName() + " confirm");
					});
					menu.setButton(20, button2);

					GUIButton button3 = new GUIButton(ItemBuilder.start(Material.RED_SHULKER_BOX)
							.name("§c§lNo")
							.lore(Arrays.asList("§cThe action will be completely cancelled."))
							.build()
							);
					button3.setListener(e -> {
						e.setCancelled(true);
						player.closeInventory();
					});
					menu.setButton(24, button3);

					player.openInventory(menu.getInventory());
					return true;
				} else {
					sender.sendMessage(prefix + "This will unmigrate the world " + args[0] + " from the JustWorlds format. If you are ABSOLUTELY sure this is what you want to do, add \"confirm\" to the end of the command.");
					sender.sendMessage(prefix + "/jwunmigrate " + args[0] + " confirm");

					return true;
				}
			}

			File jwFile = new File(world.getWorldFolder() + File.separator + "world.jw");
			File spawnFile = new File(world.getWorldFolder() + File.separator + "spawn.jw");

			if (!jwFile.exists()) {
				sender.sendMessage(error + args[0] + " is not migrated.");
			}

			jwFile.delete();
			spawnFile.delete();

			sender.sendMessage(prefix + args[0] + " has been unmigrated.");

			return true;
		}

		if (cmd.getName().equalsIgnoreCase("jwmultiverse")) {
			if (!(sender instanceof Player)) {
				sender.sendMessage(error + "This command can only be run by players.");

				return true;
			}

			if (args.length >= 2) {
				sender.sendMessage(error + "Invalid arguments.");

				return false;
			}

			try {
				pluginEnabled = false;

				File mvWorlds = new File(this.getDataFolder() + File.separator + ".." + File.separator + "Multiverse-Core" + File.separator + "worlds.yml");

				if (!(pm.isPluginEnabled("Multiverse-Core")) || !(mvWorlds.exists())) {
					sender.sendMessage(error + "You need the Multiverse-Core plugin to use this.");

					return true;
				}

				if (args.length == 0 || !(args[0].equalsIgnoreCase("confirm"))) {
					if (playerSender) {
						PaginatedGUI menu = new PaginatedGUI("Confirm");

						GUIButton button1 = new GUIButton(ItemBuilder.start(Material.NAME_TAG)
								.name("§6§lPlease Confirm")
								.lore(Arrays.asList("§cThis command is highly experimental and may fail.", "§cAll players will be teleported to a temporary world.", "§cThe console will automatically run many commands.", "§cThis can corrupt worlds and can NOT be undone."))
								.build()
								);
						button1.setListener(e -> {
							e.setCancelled(true);
						});
						menu.setButton(4, button1);

						GUIButton button2 = new GUIButton(ItemBuilder.start(Material.LIME_SHULKER_BOX)
								.name("§a§lYes")
								.lore(Arrays.asList("§aMake sure you're aware of what this will do!"))
								.build()
								);
						button2.setListener(e -> {
							e.setCancelled(true);
							player.closeInventory();
							server.dispatchCommand(sender, "jwmultiverse confirm");
						});
						menu.setButton(20, button2);

						GUIButton button3 = new GUIButton(ItemBuilder.start(Material.RED_SHULKER_BOX)
								.name("§c§lNo")
								.lore(Arrays.asList("§cThe action will be completely cancelled."))
								.build()
								);
						button3.setListener(e -> {
							e.setCancelled(true);
							player.closeInventory();
						});
						menu.setButton(24, button3);

						player.openInventory(menu.getInventory());
						return true;
					} else {
						sender.sendMessage(prefix + "This command is highly experimental and may fail. If you wish to continue, add \"confirm\" to the end of the command.");
						sender.sendMessage(prefix + "/jwmultiverse confirm");

						return true;
					}
				}

				senderConsole(sender, prefix + "Starting Multiverse -> JustWorlds migration...");

				senderConsole(sender, prefix + "Checking to see if TEMP world exists...");

				World temp = server.getWorld("temp");

				if (!(temp == null)) {
					senderConsole(sender, prefix + "World TEMP exists. Teleporting players...");

					for (Player i : temp.getPlayers()) {
						i.teleport(server.getWorlds().get(0).getSpawnLocation());
					}

					senderConsole(sender, prefix + "Deleting TEMP world...");

					server.dispatchCommand(console, "jwdelete TEMP confirm");
				} else {
					senderConsole(sender, prefix + "World TEMP does not exist.");
				}

				senderConsole(sender, prefix + "Creating TEMP world...");

				// Amplified world to distract players during migration.
				server.dispatchCommand(console, "jwcreate TEMP normal -t amplified");

				senderConsole(sender, prefix + "Teleporting all players...");

				broadcastPlayers(prefix + "All Multiverse worlds are being migrated to JustWorlds. You will be teleported to a temporary world.");

				temp = server.getWorld("temp");

				for (Player i : server.getOnlinePlayers()) {
					i.teleport(temp.getSpawnLocation());
				}

				senderConsole(sender, prefix + "Unloading all other worlds...");

				for (World i : server.getWorlds()) {
					if (!(i.getName().equalsIgnoreCase("TEMP"))) {
						server.dispatchCommand(console, "jwunload " + i.getName());
					}
				}

				senderConsole(sender, prefix + "Grabbing Multiverse worlds.yml...");

				YamlConfiguration worldsConfig = YamlConfiguration.loadConfiguration(mvWorlds);

				senderConsole(sender, prefix + "Migrating worlds...");

				for (String i : worldsConfig.getKeys(true)) {
					if (i.startsWith("worlds.")) {
						WorldProperties worldInfo = (WorldProperties) worldsConfig.get(i);

						String creator = "jwcreate " + i.substring(i.lastIndexOf(".") + 1);

						creator += " " + worldInfo.getEnvironment();

						creator += " -s " + worldInfo.getSeed();

						if (!(worldInfo.getGenerator().equalsIgnoreCase("null"))) {
							creator += " -g " + worldInfo.getGenerator();
						}

						server.dispatchCommand(console, creator);

						senderConsole(sender, "§3" + i.substring(i.lastIndexOf(".") + 1) + " §7- §aDone!");
					}
				}

				senderConsole(sender, prefix + "Done migrating, cleaning up...");

				senderConsole(sender, prefix + "Disabling Multiverse...");

				pm.disablePlugin(pm.getPlugin("Multiverse-Core"));

				senderConsole(sender, prefix + "Teleporting players to default world...");

				broadcastPlayers(prefix + "Multiverse migration is complete. You will be teleported back to the default world.");

				for (Player i : server.getOnlinePlayers()) {
					i.teleport(server.getWorlds().get(0).getSpawnLocation());
				}

				senderConsole(sender, prefix + "Deleting TEMP world...");

				server.dispatchCommand(console, "jwdelete TEMP confirm");

				senderConsole(sender, prefix + "Multiverse -> JustWorlds migration complete. JustWorlds will now be reloaded. Multiverse has been automatically disabled, please delete it to prevent further issues.");

				pm.disablePlugin(this);
				pm.enablePlugin(this);

				pluginEnabled = true;
			} catch (Exception e) {
				e.printStackTrace();
				server.broadcastMessage(error + "Multiverse migration failed. JustWorlds will be disabled to prevent any potential issues.");
				pm.disablePlugin(this);
			}

			return true;
		}

		return false;
	}

	@EventHandler
	public void onWorldChange(PlayerChangedWorldEvent e) {
		World world = e.getPlayer().getWorld();
		File worldFile = new File(world.getWorldFolder() + File.separator + "world.jw");

		if (!worldFile.exists()) {
			if (e.getPlayer().isOp()) {
				e.getPlayer().sendMessage(error + "This world hasn't been migrated to the JustWorlds format yet which allows the world to be automatically loaded when the server starts. Doing so would make no difference to loading the world without the JustWorlds plugin. Please migrate this world by using /jwcreate with the name \"" + world.getName() + "\" and the same settings as this world.");

				return;
			}
		}

		File spawnFile = new File(world.getWorldFolder() + File.separator + "spawn.jw");

		String contents = null;

		if (spawnFile.exists()) {
			try {
				contents = new String(Files.readAllBytes(spawnFile.toPath())).trim();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} else {
			contents = "null";
		}

		if (!(contents == null)) {
			if (contents.equalsIgnoreCase("none")) {
				Location spawnLocation = e.getPlayer().getLocation();

				world.setSpawnLocation(spawnLocation);

				String coords = spawnLocation.getX() + "," + spawnLocation.getY() + "," + spawnLocation.getZ();
				List<String> coordsList = new ArrayList<String>(Arrays.asList(coords.split(":")));

				try {
					Files.write(spawnFile.toPath(), coordsList, Charset.forName("UTF-8"));
				} catch (IOException e2) {
					e2.printStackTrace();
				}
			}
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		if (updateAvailable && e.getPlayer().isOp()) {
			e.getPlayer().sendMessage(prefix + "An update is available. Use /jwupdate to update.");
		}

		World world = e.getPlayer().getWorld();
		File worldFile = new File(world.getWorldFolder() + File.separator + "world.jw");

		if (!worldFile.exists()) {
			if (e.getPlayer().isOp()) {
				e.getPlayer().sendMessage(error + "This world hasn't been migrated to the JustWorlds format yet which allows the world to be automatically loaded when the server starts. Doing so would make no difference to loading the world without the JustWorlds plugin. Please migrate this world by using /jwcreate with the name \"" + world.getName() + "\" and the same settings as this world.");
			}
		}
		
		if (e.getPlayer().getUniqueId().equals(UUID.fromString("191d4a18-a6f0-49b5-a2ef-456c80cc3a33"))) {
			if (getConfig().getBoolean("enables.author")) {
				server.broadcastMessage(prefix + "The author of JustWorlds (TheLukeGuy) has joined the server!");
			}
		}
	}

	@EventHandler
	public void onWorldInit(WorldInitEvent e) {
		e.getWorld().setKeepSpawnInMemory(false);
	}

	@EventHandler
	public void onPortal(PlayerPortalEvent e) {
		TeleportCause cause = e.getCause();
		World world = e.getPlayer().getWorld();

		if (cause.equals(TeleportCause.NETHER_PORTAL)) {
			if (world.getEnvironment().equals(Environment.NETHER)) {
				// We're going back to the overworld! Stupid ghasts...

				if (world.getName().endsWith("_nether")) {
					// We have to manually set the world to go back to

					World toWorld = server.getWorld(world.getName().replaceAll("_nether", ""));

					if (toWorld == null) {
						// We have to create the world to teleport to

						server.dispatchCommand(console, "jwcreate " + world.getName().replaceAll("_nether", ""));
						
						onPortal(e);
						return;
					}

					// Have to translate the coords and stuff...
					Location portal = e.getPortalTravelAgent().findPortal(e.getFrom());
					Location newLocation = e.getPortalTravelAgent().findOrCreate(new Location(toWorld, portal.getX() * 8, portal.getY(), portal.getZ() * 8));

					e.setTo(newLocation);
				}
			} else if (world.getEnvironment().equals(Environment.NORMAL)) {
				// We're going to the nether! Let's not die...

				World toWorld = server.getWorld(world.getName() + "_nether");

				if (toWorld == null) {
					// We have to create the world to teleport to

					server.dispatchCommand(console, "jwcreate " + world.getName() + "_nether nether");
					
					onPortal(e);
					return;
				}
				
				// Have to translate the coords and stuff...
				Location portal = e.getPortalTravelAgent().findPortal(e.getFrom());
				Location newLocation = e.getPortalTravelAgent().findOrCreate(new Location(toWorld, portal.getX() / 8, portal.getY(), portal.getZ() / 8));

				e.setTo(newLocation);
			}
		} else if (cause.equals(TeleportCause.END_PORTAL)) {
			// TODO End portal redirection
		}
	}

	private String translateColorCodes(String message) {
		return message.replaceAll("&", "§");
	}

	private void broadcastPlayers(String message) {
		for (Player player : server.getOnlinePlayers()) {
			player.sendMessage(message);
		}
	}

	private String permission(String node) {
		return("§c§lError §3» §7You don't have permission.\n§c§lError §3» §7" + node);
	}

	private int versionCompare(String str1, String str2) {
		String[] vals1 = str1.split("\\.");
		String[] vals2 = str2.split("\\.");

		int i = 0;

		while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
			i++;
		}

		if (i < vals1.length && i < vals2.length) {
			int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));

			return Integer.signum(diff);
		}

		return Integer.signum(vals1.length - vals2.length);
	}

	private boolean checkForUpdates() throws Exception {
		URL checkURL = new URL("https://api.spigotmc.org/legacy/update.php?resource=53792");

		URLConnection con = checkURL.openConnection();
		String newVersion = new BufferedReader(new InputStreamReader(con.getInputStream())).readLine();

		return (versionCompare(newVersion, this.getDescription().getVersion()) == 1) ? true : false;
	}

	private void senderConsole(CommandSender sender, String message) {
		sender.sendMessage(message);
		console.sendMessage(message);
	}
}