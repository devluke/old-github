from __future__ import print_function
from easygui import *
import random
import os

# Simple responses (call random.choice(list))
greetings = ["Hello!", "Hey!", "Hi!", "What's up?"]
greetings_c = ["Hello,", "Hey,", "Hi,", "What's up,"]
dontknow = ["I don't seem to know", "I'm not sure I know", "I don't know", "I'm not sure"]
whats = ["What's", "What is", "Could you tell me"]
howareyou = ["How are you today?", "How are you?"]
howareyou_c = ["how are you today?", "how are you?"]
whatmakesit = ["What makes it", "Why", "Why would you say"]
good = ["Awesome!", "Cool!", "Great!", "Sweet!", "That's cool!"]
fair = ["Alright.", "Okay.", "That's good.", "Cool."]
bad = ["That stinks.", "Aw.", "That's not good.", "Sorry to hear that."]

print("opening z01ky")

if not os.path.isfile("data/boot.txt"):
    open("data/boot.txt", "a").close()
    with open("data/name.txt", "w+") as nf:
        nf.write(enterbox("{} I'm Zoik! {} who you are! {} your name?".format(random.choice(greetings_c), random.choice(dontknow), random.choice(whats)), "z01ky"))
        nf.close()

with open("data/name.txt", "r") as nf:
    name = nf.read()
    nf.close()

# IGNORE THIS COMMENT # map(''.join, itertools.product(*((c.upper(), c.lower()) for c in 'STRING GOES HERE')))
# IGNORE THIS COMMENT # if num % 2 == 0:

# And this is where the conversation starts... :o

z_howareyou = enterbox("{} {}! {}".format(random.choice(greetings_c), name, random.choice(howareyou)), "z01ky")

zl_howareyou = z_howareyou.split()
zw_howareyou_good = ["good", "great", "amazing", "fabulous", "fantastic", "super", "fine", "epic", "cool", "gr8"]
zw_howareyou_fair = ["okay", "ok", "fair", "meh"]
zw_howareyou_bad = ["bad", "terrible", "horrible", "bleh", "blah"]
zc_howareyou_good = 0
zc_howareyou_fair = 0
zc_howareyou_bad = 0
zc_howareyou_nots = 0
za_howareyou = ""
for i in zl_howareyou:
    if i in zw_howareyou_good:
        zc_howareyou_good += 1
for i in zl_howareyou:
    if i in zw_howareyou_fair:
        zc_howareyou_fair += 1
for i in zl_howareyou:
    if i in zw_howareyou_bad:
        zc_howareyou_bad += 1
for i in zl_howareyou:
    if i == "not":
        zc_howareyou_nots += 1
if zc_howareyou_nots > 0:
    if zc_howareyou_nots % 2 == 0:
        zdn_howareyou = True
    else:
        zdn_howareyou = False
    zn_howareyou = True
else:
    zn_howareyou = False
zt_howareyou = [zc_howareyou_good, zc_howareyou_fair, zc_howareyou_bad]
zi_howareyou = zt_howareyou.index(max(zt_howareyou))
if zi_howareyou == 0:
    za_howareyou = "good"
elif zi_howareyou == 1:
    za_howareyou = "fair"
else:
    za_howareyou = "bad"
if zn_howareyou:
    if not zdn_howareyou:
        if za_howareyou in ["good", "fair"]:
            za_howareyou = "bad"
        else:
            za_howareyou = "good"

if za_howareyou == "good":
    enterbox("{} {} good?".format(random.choice(good), random.choice(whatmakesit)))
elif za_howareyou == "fair":
    enterbox("{} {} fair?".format(random.choice(fair), random.choice(whatmakesit)))
else:
    enterbox("{} {} bad?".format(random.choice(bad), random.choice(whatmakesit)))
