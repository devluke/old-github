package net.lukepchambers.ChatEvents;

import java.util.List;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommandYamlParser;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class ChatEvents extends JavaPlugin {
	Logger log = Logger.getLogger("Minecraft");
	public void onEnable() {
		saveDefaultConfig();
		Bukkit.getConsoleSender().sendMessage("§6ChatEvents §cenabled§6!");
	}
	public void onDisable() {
		Bukkit.getConsoleSender().sendMessage("§6ChatEvents §cdisabled§6!");
	}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("chatevents")) {
			if (args.length == 0) {
				return false;
			} else if (args[0].equalsIgnoreCase("about")) {
				sender.sendMessage(ChatColor.GOLD + "This server uses ChatEvents version " + ChatColor.RED + getDescription().getVersion() + ChatColor.GOLD + " by " + ChatColor.RED + "TheLukeGuy");
				sender.sendMessage("");
				sender.sendMessage(ChatColor.GOLD + "Credit also goes to the players on my Minecraft server for giving me some ideas for commands");
				sender.sendMessage("");
				sender.sendMessage(ChatColor.GOLD + "Use §c/ce help §6for a list of commands");
			} else if (args[0].equalsIgnoreCase("help")) {
				for (Plugin plugin : Bukkit.getPluginManager().getPlugins()) {
	                if (plugin.getName().equals("ChatEvents")) {
	                    List<Command> cmdList = PluginCommandYamlParser.parse(plugin);
	                    sender.sendMessage(ChatColor.GOLD + "List of ChatEvents commands (" + cmdList.size() + "):");
	                    for(int i=0; i < cmdList.size(); i++) {
	                        sender.sendMessage(ChatColor.GOLD + "/" + cmdList.get(i).getName().toString());
	                    }
	                }
	            }
			} else {
				return false;
			}
		}
		
		if (cmd.getName().equalsIgnoreCase("hate")) {
			Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " hates " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("love")) {
			Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + sender.getName() + " loves " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("rage")) {
			Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " rages " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("kiss")) {
			Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + sender.getName() + " kisses " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("hug")) {
			Bukkit.broadcastMessage(ChatColor.LIGHT_PURPLE + sender.getName() + " hugs " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("punch")) {
			Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " punches " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("murder")) {
			Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " murders " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("like")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " likes " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("slap")) {
			Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " slaps " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("build")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " builds " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("mine")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " mines " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("attack")) {
			Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " attacks " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("grief")) {
			Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " griefs " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("steal")) {
			Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " steals " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("ask")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " asks " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("want")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " wants " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("need")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " needs " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("facepalm")) {
			Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " facepalms " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("dab")) {
			Bukkit.broadcastMessage(ChatColor.GREEN + sender.getName() + " dabs!");
		}
		if (cmd.getName().equalsIgnoreCase("fidget")) {
			Bukkit.broadcastMessage(ChatColor.GREEN + sender.getName() + " spins a fidget spinner!");
		}
		if (cmd.getName().equalsIgnoreCase("boneless")) {
			Bukkit.broadcastMessage(ChatColor.GREEN + sender.getName() + " asks for a boneless pizza!");
		}
		if (cmd.getName().equalsIgnoreCase("poke")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " pokes " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("highfive")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " high fives " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("yell")) {
			Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " yells " + String.join(" ", args).toUpperCase());
		}
		if (cmd.getName().equalsIgnoreCase("bite")) {
			Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " bites " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("snuggle")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " snuggles " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("shake")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " shakes " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("stab")) {
			Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " stabs " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("cry")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " cries " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("laugh")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " laughs " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("think")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " thinks " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("sing")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " sings " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("run")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " runs " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("walk")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " walks " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("dislike")) {
			Bukkit.broadcastMessage(ChatColor.RED + sender.getName() + " dislikes " + String.join(" ", args));
		}
		if (cmd.getName().equalsIgnoreCase("jeff")) {
			Bukkit.broadcastMessage(ChatColor.GREEN + sender.getName() + " thinks their name is Jeff!");
		}
		if (cmd.getName().equalsIgnoreCase("pepe")) {
			Bukkit.broadcastMessage(ChatColor.GREEN + sender.getName() + " likes Pepe the frog!");
		}
		if (cmd.getName().equalsIgnoreCase("trump")) {
			Bukkit.broadcastMessage(ChatColor.GREEN + sender.getName() + " wants to build a wall!");
			Bukkit.broadcastMessage(ChatColor.GREEN + sender.getName() + " wants Mexico to pay for the wall!");
		}
		if (cmd.getName().equalsIgnoreCase("thank")) {
			Bukkit.broadcastMessage(ChatColor.GOLD + sender.getName() + " thanks " + String.join(" ", args));
		}
		return true;
	}
}
