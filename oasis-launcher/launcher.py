from __future__ import print_function
import os
import sys
import urllib
import zipfile
import subprocess
import glob
import shutil

def log(text, texttype):
    print("[{}] {}".format(texttype.upper(), text))
    if texttype == "critical":
        sys.exit(1)

def clear():
    os.system("clear")
    pass

try:
    argument = sys.argv[1]
except IndexError:
    argument = None

try:
    import easygui
except ImportError:
    log("Missing EasyGUI module", "critical")

if argument == "manage":
    while True:
        manage = easygui.choicebox("Choose an action.", "Manage Versions", ["Delete", "Reset"])
        if not manage:
            sys.exit(0)
        if manage == "Delete":
            log("Searching for downloaded versions", "info")
            todelete = glob.glob("versions/*")
            todelete.remove("versions/empty.txt")
            todelete = [os.path.basename(str) for str in todelete]
            log("Versions {} found".format(", ".join(todelete)), "info")
            vtodelete = easygui.choicebox("Delete which version?", "Manage Versions", todelete)
            if not vtodelete:
                pass
            log("Version {} selected".format(vtodelete), "info")
            log("Attempting to delete version {}".format(vtodelete), "info")
            shutil.rmtree("versions/{}".format(vtodelete))
            log("Successfully deleted version {}".format(vtodelete), "info")
        elif manage == "Reset":
            log("Searching for downloaded versions", "info")
            toreset = glob.glob("versions/*")
            toreset.remove("versions/empty.txt")
            toreset = [os.path.basename(str) for str in toreset]
            log("Versions {} found".format(", ".join(toreset)), "info")
            vtoreset = easygui.choicebox("Reset which version?", "Manage Versions", toreset)
            if not vtoreset:
                pass
            log("Version {} selected".format(vtoreset), "info")
            log("Attempting to reset version {}".format(vtoreset), "info")
            subprocess.call("cd versions/{}; python reset.py; cd ../..".format(vtoreset), shell=True)

versions = ["3.2.1", "3.2", "3.1", "3.0", "2.2.1", "2.1.1", "2.1", "2.0.1", "pre-1.1"]

clear()
log("oasis Launcher v0.1.1", "info")
version = easygui.choicebox("Select version to launch.", "Launcher v0.1.1", versions)
if version not in versions:
    sys.exit(0)
log("Version {} selected".format(version), "info")
log("Checking if version is downloaded to versions folder", "info")
if os.path.isfile("versions/{}/main.py".format(version)):
    log("Version is in versions folder", "info")
else:
    log("Version is not in versions folder", "info")
    log("Attempting to download version {}".format(version), "info")
    if not version in ["3.1", "pre-1.1"]:
        urllib.urlretrieve("https://github.com/TheLukeGuy/oasis/archive/{}.zip".format(version), "versions/{}.zip".format(version))
    elif version == "pre-1.1":
        urllib.urlretrieve("https://www.dropbox.com/s/52fp3d7j60btm5f/oasis-pre-1.1.zip?dl=1", "versions/pre-1.1.zip")
    else:
        urllib.urlretrieve("https://github.com/TheLukeGuy/oasis/archive/3.1-fix2.zip", "versions/3.1.zip")
    log("Successfully downloaded version {}".format(version), "info")
    log("Attempting to unzip file", "info")
    zip = zipfile.ZipFile('versions/{}.zip'.format(version))
    zip.extractall()
    log("Successfully unzipped file", "info")
    log("Moving file to versions directory", "info")
    if not version in ["3.1", "pre-1.1"]:
        os.rename("oasis-{}".format(version), "versions/{}".format(version))
    elif version == "pre-1.1":
        os.rename("oasis-pre-1.1", "versions/pre-1.1")
        log("Resetting pre-1.1", "info")
        subprocess.call("cd versions/pre-1.1; python reset.py; cd ../..", shell=True)
    else:
        os.rename("oasis-3.1-fix2", "versions/3.1")
    log("Cleaning up", "info")
    os.remove("versions/{}.zip".format(version))
log("Attempting to launch oasis {}".format(version), "info")
subprocess.call("cd versions/{}; python main.py".format(version), shell=True)
