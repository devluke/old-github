pyenv
=====

pyenv is a simple Python virtualenv wrapper (BASH shell script) to simplify
managing virtualenv environments.

Installation
============

Copy pyenv to your home directory and add to your .bash_profile file::

    if [ -f ${HOME}/pyenv ]; then
        source ${HOME}/pyenv
    fi

Usage
=====

::

    Usage: pyenv command [virtualenv options] | <venv>

    Available options are:
        list          List virtual environments
        create <venv> Create virtual environment <venv>
        delete <venv> Delete virtual environment <venv>
        help | -h     This help text

    VIRTUALENV_HOME is /home/pyenv/.virtualenvs

Notes
=====

* To keep it as simple as possible, none of the options ask for verification (which, at this point, is applicable only to deleting a virtual environment).

* VIRTUALENV_HOME is set to ${HOME}/.virtualenvs to more easily tie-in with `Sasha Hart's Vex Utility <https://github.com/sashahart/vex.git>`_

License
=======

See LICENSE file.
