# Carbon
The code behind Carbon
### Building
* Create a database with `sqlite3 database.db < schema.sql`
* Rename config.py-sample to config.py and edit to your liking
* With Flask, Flask-Mail, Flask-Login, and Flask-WTF installed, run `./app.py`
