#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import *
from flask_login import *
from flask_mail import *
from wtforms import *
from wtforms.validators import *
from models import *
import sqlite3
import hashlib
import uuid

app = Flask(__name__)
app.config.from_object("config")
mail = Mail(app)

login_manager = LoginManager()
login_manager.init_app(app)

class User(UserMixin):
    pass

class RegistrationForm(Form):
    username = StringField("Username", [validators.Length(min=4, max=25)])
    email = StringField("Email Address", [validators.Length(min=6, max=35), Email()])
    password = PasswordField("New Password", [
        validators.DataRequired(),
        validators.EqualTo("confirm", message="Passwords must match"),
        validators.Length(min=6)
    ])
    confirm = PasswordField("Repeat Password")
    
class ContactForm(Form):
    contact_name = StringField("Name:")
    contact_email = StringField("Email:", [Email()])
    contact_subject = StringField("Subject:")
    contact_message = TextAreaField("Message")
    
def hash_password(password):
    salt = uuid.uuid4().hex
    return hashlib.sha256(salt.encode() + password.encode()).hexdigest() + ":" + salt
    
def check_password(hashed_password, user_password):
    password, salt = hashed_password.split(":")
    return password == hashlib.sha256(salt.encode() + user_password.encode()).hexdigest()

@login_manager.user_loader
def user_loader(username):
    if check_by_username_account_holder(username):
        return

    user = User()
    user.id = username
    return user


@login_manager.request_loader
def request_loader(request):
    username = request.form.get("username")
    if not check_by_username_account_holder(username):
        return

    user = User()
    user.id = username

    user.is_authenticated = check_password(select_by_username_account_holder(username)[0][2], request.form["password"])

    return user

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/editor")
def editor():
    return render_template("editor.html")

@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "GET":
        return render_template("login.html")

    username = request.form["username"]
    if not current_user.is_authenticated:
        try:
            if check_password(select_by_username_account_holder(username)[0][2], request.form["password"]):
                user = User()
                user.id = username
                login_user(user, remember=True)
                flash("Logged in successfully", "success")
                return redirect("/")
        except:
            pass
    
        flash("Invalid username or password", "danger")
        return render_template("login.html")
    else:
        flash("You're already logged in", "danger")
        return redirect("/")

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out successfully", "success")
    return redirect("/")

@app.route("/signup", methods=["GET", "POST"])
def signup():
    form = RegistrationForm(request.form)
    
    if request.method == "GET":
        return render_template("signup.html", form=form)

    if form.validate():
        try:
            if not current_user.is_authenticated:
                insert_account_holder(request.form["email"], request.form["username"], hash_password(request.form["password"]))
                flash("Signed up successfully. Welcome, {}!".format(request.form["username"]), "success")
                user = User()
                user.id = request.form["username"]
                login_user(user, remember=True)
                return redirect("/")
        except sqlite3.IntegrityError:
            flash("That username is already taken", "danger")
    else:
        flash("Please review the form and try again", "danger")
    return render_template("signup.html", form=form)
    
@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/rules")
def rules():
    return render_template("rules.html")

@app.route("/credits")
def credits():
    return render_template("credits.html")

@app.route("/terms")
def terms():
    return render_template("terms.html")

@app.route("/privacy")
def privacy():
    return render_template("privacy.html")

@app.route("/copyright")
def copyright():
    return render_template("copyright.html")

@app.route("/contact", methods=["GET", "POST"])
def contact():
    form = ContactForm(request.form)
    
    if request.method == "POST":
        if form.validate():
            try:
                msg = Message(request.form["contact_subject"], sender=(request.form["contact_name"], request.form["contact_email"]), recipients=["lpc@lukepchambers.net"], body=request.form["contact_message"])
                msg.html = msg.body
                mail.send(msg)
                flash("Your message has been sent and we'll reply within 48 hours", "success")
            except:
                flash("There was a problem sending your message", "danger")
            return redirect("/")
        else:
            flash("Please enter a valid email", "danger")
    
    return render_template("contact.html", form=form)

@app.route("/doggles")
def doggles():
    flash("Thank you for checking Carbon out and thank you to Doggles for referring you!", "success")
    return redirect("/")

@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404

@login_manager.unauthorized_handler
def unauthorized_handler():
    return render_template("login.html")

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")