import sqlite3 as sql

# account_holder
def insert_account_holder(email,username,password):
    with sql.connect("database.db") as con:
        cur = con.cursor()
        cur.execute("INSERT INTO account_holder (email,username,password) VALUES (?,?,?)", (email,username,password) )
        con.commit()  

def select_account_holder(params=()):
    with sql.connect("database.db") as con:
        cur = con.cursor()
        if params==():
            cur.execute("select * from account_holder")
        else:
            string = "select "
            for i in xrange(len(params)-1):
                string += "%s,"
            string += "%s"
            string += " from account_holder"
            print string
            result = cur.execute(string % params)
    return result.fetchall()

def select_by_username_account_holder(username):
    with sql.connect("database.db") as con:
        cur = con.cursor()
        result = cur.execute("select * from account_holder where username='%s';" % username).fetchall()
    return result

def check_by_username_account_holder(username):
    with sql.connect("database.db") as con:
        cur = con.cursor()
        result = cur.execute("select * from account_holder where username='%s';" % username).fetchall() is None
    return result


# projects
def insert_projects(name,username,id,description,short):
    with sql.connect("database.db") as con:
        cur = con.cursor()
        cur.execute("INSERT INTO projects (name,username,id,description,short) VALUES (?,?,?,?,?)", (name,username,id,description,short) )
        con.commit()

def select_projects(params=()):
    with sql.connect("database.db") as con:
        cur = con.cursor()
        if params==():
            cur.execute("select * from projects")
        else:
            string = "select "
            for i in xrange(len(params)-1):
                string += "%s,"
            string += "%s"
            string += " from projects"
            print string
            result = cur.execute(string % params)
    return result.fetchall()

def select_by_id_projects(id):
    with sql.connect("database.db") as con:
        cur = con.cursor()
        result = cur.execute("select * from projects where id='%s';" % id).fetchall()
    return result

def check_by_id_projects(id):
    with sql.connect("database.db") as con:
        cur = con.cursor()
        result = cur.execute("select * from projects where id='%s';" % id).fetchall() is None
    return result