drop table if exists account_holder;
create table account_holder (
    email text not null,
    username text primary key not null,
    password text null
);

drop table if exists projects;
create table projects {
    name text not null,
    username text not null,
    id int primary key not null,
    description text not null,
    short text not null
}