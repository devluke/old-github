#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, render_template

app = Flask(__name__)
app.config.from_object(__name__)


@app.route('/')
def index():
	return render_template('index.html', section_title='Luke Chambers')

@app.route('/about')
def about():
	return render_template('about.html', section_title="About Me")

@app.route('/projects/scratch')
def scratch():
	return render_template('scratch.html', section_title="Scratch")

@app.route('/ios')
def ios():
	return render_template('ios.html', section_title="iOS Hacked Apps")

@app.route('/ios/error')
def iosonly():
	return render_template('iosonly.html', section_title="iOS Only")

@app.route('/doss')
def doss():
	return render_template('doss3.html', section_title="Dragon of Smelted Sand v3")

@app.route('/oasis/books/stevealonedead')
def stevealonedead():
	return render_template('oasisbook-stevealonedead.html', section_title="oasis Book - Steve, Alone, Dead")

@app.route('/oasis/books/thereturnofsteve')
def thereturnofsteve():
	return render_template('oasisbook-thereturnofsteve.html', section_title="oasis Book - The Return of Steve")

@app.route('/oasis/books/lastbreath')
def lastbreath():
	return render_template('oasisbook-lastbreath.html', section_title="oasis Book - Last Breath")

@app.route('/oasis')
def oasis():
	return render_template('oasis.html', section_title="oasis")

@app.route('/text-bg')
def textbg():
	return render_template('text-bg.html', section_title="Text Background")

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
