package net.lukepchambers.justworlds2

import org.bukkit.*
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerPortalEvent
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause
import org.bukkit.event.world.WorldInitEvent
import org.bukkit.plugin.java.JavaPlugin
import java.io.File
import java.util.*

@Suppress("unused")
class JustWorlds : JavaPlugin(), Listener {

    private val svr = Bukkit.getServer()
    private val console = svr.consoleSender
    private val pm = svr.pluginManager

    private var prefix = "${ChatColor.DARK_AQUA}✔ ${ChatColor.AQUA}JustWorlds ${ChatColor.DARK_AQUA}»${ChatColor.GRAY}"
    private var error = "${ChatColor.DARK_RED}✘ ${ChatColor.RED}JustWorlds ${ChatColor.DARK_RED}»${ChatColor.GRAY}"

    private var invalidArguments = "$error Invalid arguments."
    private var unknownWorld = "$error Unknown world. Use /jw list for a list of worlds."
    private var unknownPlayer = "$error Unknown player."
    private var invalidConsoleArguments = "$error You can't use that set of arguments as the console."
    private var otherPermission = "$error You don't have permission to use this command on other users."
    private var invalidPage = "$error Invalid page number. Valid pages numbers:"
    private var notMigrated = "$error The requested world has not yet been migrated to JustWorlds."

    private val sep = File.separator
    private val root = File("${dataFolder.path}$sep..$sep..").toPath().toString()

    private var configVersion = 2

    override fun onEnable() {

        pm.registerEvents(this, this)

        @Suppress("UNUSED_VARIABLE")
        val metrics = Metrics(this)

        saveDefaultConfig()

        if (config.getInt("version") < configVersion) {

            console.sendMessage("$prefix Resetting config.yml...")

            val oldConfig = YamlConfiguration.loadConfiguration(File(dataFolder, "config.yml"))

            File(dataFolder, "config.yml").delete()
            saveDefaultConfig()
            reloadConfig()

            oldConfig.getKeys(true).forEach {

                if (config.contains(it) && it != "version") config.set(it, oldConfig.get(it))

            }

            saveConfig()

        }

        val starts = config.getInt("starts")

        config.set("starts", starts + 1)
        saveConfig()

        if (starts == 1) {

            val worlds = findWorlds()

            console.sendMessage("$prefix Beginning migration to JustWorlds...")
            console.sendMessage("$prefix Checking for JustWorlds <= 2.0.1 (old) worlds...")

            val oldWorlds = worlds.filter { worldType(it) == "old" }

            if (oldWorlds.isNotEmpty()) {

                console.sendMessage("$prefix Migrating old worlds...")

                oldWorlds.forEach {

                    val jwFile = File("$it${sep}world.jw").readText().trim()
                    val spawnFile = File("$it${sep}spawn.jw").readText().trim()

                    var jwCreate = jwFile.replace("`colon`", ":").split(" ").toMutableList()
                    var spawnCreate = spawnFile.split(",")

                    jwCreate = jwCreate.map { it.trim() }.toMutableList()
                    spawnCreate = spawnCreate.map { it.trim() }

                    if (jwCreate.size > 2) {

                        if (jwCreate[0] == "jwcreate") jwCreate.add(2, "-e")
                        else jwCreate.add(3, "-e")

                        if (jwCreate.contains("-t") && jwCreate[jwCreate.indexOf("-t") + 1] == "empty") {

                            jwCreate.removeAt(jwCreate.indexOf("-t"))
                            jwCreate.removeAt(jwCreate.indexOf("-t") + 1)

                        }

                    }

                    svr.dispatchCommand(console, jwCreate.joinToString(" ").trim())

                    val newJwFile = File("$it${sep}justworlds.yml")
                    val newJwConfig = YamlConfiguration.loadConfiguration(newJwFile)

                    if (spawnCreate.size == 3) {

                        newJwConfig.set("world.spawn.x", spawnCreate[0])
                        newJwConfig.set("world.spawn.y", spawnCreate[1])
                        newJwConfig.set("world.spawn.z", spawnCreate[2])

                    }

                    newJwConfig.save(newJwFile)

                }

                console.sendMessage("")
                console.sendMessage("${ChatColor.RED}JustWorlds just migrated all your old worlds to the new JustWorlds format.")
                console.sendMessage("${ChatColor.RED}To make sure all worlds can be loaded properly, your server has been stopped.")
                console.sendMessage("")
                console.sendMessage("${ChatColor.DARK_AQUA}You can start your server back up whenever you're ready. Enjoy the plugin!")
                console.sendMessage("")

                System.exit(0)

            }

        }

        if (starts == 0) {

            console.sendMessage("")
            console.sendMessage("${ChatColor.RED}This is your first time loading JustWorlds on this server.")
            console.sendMessage("${ChatColor.RED}To make sure config.yml is initialized properly, your server has been stopped.")
            console.sendMessage("")
            console.sendMessage("${ChatColor.DARK_AQUA}You can start your server back up whenever you're ready.")
            console.sendMessage("${ChatColor.DARK_AQUA}After this, your server will restart one more time.")
            console.sendMessage("")

            System.exit(0)

        }

        console.sendMessage("$prefix Searching for and loading worlds...")

        findWorlds().forEach {

            if (worldType(it, false) == "migrated") {

                val jwFile = File("$root$sep${it.name}${sep}justworlds.yml")
                val jwConfig = YamlConfiguration.loadConfiguration(jwFile)

                val wc = WorldCreator(jwConfig.getString("world.name"))

                wc.environment(World.Environment.valueOf(jwConfig.getString("world.environment")))
                if (jwConfig.getString("world.generator") != "null") wc.generator(jwConfig.getString("world.generator"))
                wc.seed(jwConfig.getString("world.seed").toLong())
                wc.type(WorldType.valueOf(jwConfig.getString("world.type")))

                wc.createWorld()

                val world = svr.getWorld(it.name)

                world.setSpawnLocation(jwConfig.getInt("world.spawn.x"), jwConfig.getInt("world.spawn.y"), jwConfig.getInt("world.spawn.z"))

            }

        }

        prefix = ChatColor.translateAlternateColorCodes('&', config.getString("messages.prefix"))
        error = ChatColor.translateAlternateColorCodes('&', config.getString("messages.error"))

        console.sendMessage("")
        console.sendMessage("${ChatColor.DARK_AQUA}JustWorlds version ${description.version} by TheLukeGuy.")
        console.sendMessage("${ChatColor.GREEN}Plugin enabled.")
        console.sendMessage("")

    }

    override fun onDisable() {

        console.sendMessage("")
        console.sendMessage("${ChatColor.DARK_AQUA}JustWorlds version ${description.version} by TheLukeGuy.")
        console.sendMessage("${ChatColor.RED}Plugin disabled.")
        console.sendMessage("")

    }

    override fun onCommand(sender: CommandSender, cmd: Command, label: String, args: Array<String>): Boolean {

        val command = cmd.name
        val permission = "justworlds.command.$command"

        if (!sender.hasPermission(permission)) {

            sender.sendMessage("$error You don't have permission to execute this command.")
            sender.sendMessage("$error $permission")
            return true

        }

        // justworlds [subcommand] [args]
        if (command.equals("justworlds", true)) {

            if (args.isEmpty()) {

                sender.sendMessage("$prefix JustWorlds version ${description.version} by TheLukeGuy.")
                sender.sendMessage("$prefix For help, use /$label help.")

                return true

            }

            var subcommand = "jw${args[0]}"

            args.toMutableList().subList(1, args.size).forEach { subcommand += " $it" }

            if (!description.commands.containsKey("jw${args[0]}")) {

                sender.sendMessage("$error Invalid subcommand. Use /jw help for a list of commands.")
                return true

            }

            svr.dispatchCommand(sender, subcommand)

            return true

        }

        // jwhelp [page]
        if (command.equals("jwhelp", true)) {

            if (args.size !in 0..1) {

                sender.sendMessage(invalidArguments)
                sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                return true

            }

            val commands = description.commands.keys.toList()

            var page = 1
            val lastPage = ((Math.ceil(commands.size / 8.0) * 8) / 8).toInt()

            if (args.size == 1) {

                if (args[0].toIntOrNull() == null) {

                    sender.sendMessage("$invalidPage 1-$lastPage")
                    return true

                }

                page = args[0].toInt()

            }

            if (page > lastPage || page == 0) {

                sender.sendMessage("$invalidPage 1-$lastPage")
                return true

            }

            val firstIndex = 8 * (page - 1)
            val secondIndex = if (page != lastPage) page * 8 else commands.size

            val pageCommands = commands.subList(firstIndex, secondIndex)

            sender.sendMessage("$prefix List of JustWorlds commands:")

            pageCommands.forEach {

                val hasPermission = sender.hasPermission("justworlds.command.$it")
                val commandDescription = svr.getPluginCommand(it).description

                sender.sendMessage("${if (hasPermission) ChatColor.GREEN else ChatColor.RED}/$it ${ChatColor.GRAY}- $commandDescription")

            }

            sender.sendMessage("$prefix You are viewing page $page of $lastPage.")

        }

        // jwcreate <name> [-e <environment>] [-g <generator>] [-s <seed>] [-t <type>]
        if (command.equals("jwcreate", true)) {

            if (args.size !in 1..9) {

                sender.sendMessage(invalidArguments)
                sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                return true

            }

            val argList = args.toMutableList().subList(1, args.size)

            if (argList.size % 2 != 0) {

                sender.sendMessage(invalidArguments)
                sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                return true

            }

            val argMap = argList.chunked(2) { (switch, value) -> switch to value }.toMap()

            val name = args[0]
            var environment = "NORMAL"
            var generator = "null"
            var seed = Random().nextLong().toString()
            var type = "NORMAL"

            val usedArgs = mutableListOf<String>()

            argMap.keys.forEach {

                if (usedArgs.contains(it)) {

                    sender.sendMessage(invalidArguments)
                    sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                    return true

                }

                usedArgs.add(it)

                val value = argMap[it].toString()

                when (it) {

                    "-e" -> {

                        if (value.toUpperCase() in listOf("NORMAL", "NETHER", "THE_END")) { environment = value.toUpperCase() }

                        else {

                            sender.sendMessage("$error Invalid environment. Options are: NORMAL, NETHER, THE_END")
                            return true

                        }

                    }

                    "-g" -> { generator = value }

                    "-s" -> {

                        seed = if (value.toLongOrNull() == null) { value.hashCode().toString() }
                        else { value }

                    }

                    "-t" -> {

                        if (value.toUpperCase() in listOf("NORMAL", "FLAT", "LARGE_BIOMES", "AMPLIFIED")) { type = value.toUpperCase() }

                        else {

                            sender.sendMessage("$error Invalid world type. Options are: NORMAL, FLAT, LARGE_BIOMES, AMPLIFIED")
                            return true

                        }

                    }

                }

            }

            if (File("$root$sep$name").exists()) {

                sender.sendMessage("$prefix A file or world with that name already exists.")
                return true

            }

            sender.sendMessage("$prefix Creating a new world...")

            val wc = WorldCreator(name)

            wc.environment(World.Environment.valueOf(environment))
            if (generator != "null") wc.generator(generator)
            wc.seed(seed.toLong())
            wc.type(WorldType.valueOf(type))

            wc.createWorld()

            val world = svr.getWorld(name)
            val jwFile = File("$root$sep${world.name}${sep}justworlds.yml")
            val jwConfig = YamlConfiguration.loadConfiguration(jwFile)

            jwConfig.set("world.name", name)
            jwConfig.set("world.environment", environment)
            jwConfig.set("world.generator", generator)
            jwConfig.set("world.seed", seed)
            jwConfig.set("world.type", type)
            jwConfig.set("world.spawn.x", world.spawnLocation.x)
            jwConfig.set("world.spawn.y", world.spawnLocation.y)
            jwConfig.set("world.spawn.z", world.spawnLocation.z)

            jwConfig.save(jwFile)

            sender.sendMessage("$prefix Successfully created $name. Use /jw visit $name to visit the world.")

            return true

        }

        // jwvisit <world> [player]
        if (command.equals("jwvisit", true)) {

            if (args.size == 1 && sender !is Player) {

                sender.sendMessage(invalidConsoleArguments)
                sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                return true

            }

            if (args.size !in 1..2) {

                sender.sendMessage(invalidArguments)
                sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                return true

            }

            val world = svr.getWorld(args[0])

            if (world == null) {

                sender.sendMessage(unknownWorld)
                return true

            }

            val spawnLocation = world.spawnLocation
            lateinit var player: Player

            if (args.size == 2) {

                if (!sender.hasPermission("justworlds.command.jwvisit.other")) {

                    sender.sendMessage(otherPermission)
                    sender.sendMessage("$error justworlds.command.jwvisit.other")
                    return true

                }

                if (svr.getPlayer(args[1]) == null) {

                    sender.sendMessage(unknownPlayer)
                    return true

                }

                player = svr.getPlayer(args[1])

            } else { player = sender as Player }

            player.teleport(spawnLocation)

            if (args.size == 1) { sender.sendMessage("$prefix You've been teleported to ${world.name}.") }

            else {

                sender.sendMessage("$prefix You teleported ${player.name} to ${world.name}.")
                player.sendMessage("$prefix You've been teleported to ${world.name} by ${player.name}.")

            }

            return true

        }

        // jwworld [player]
        if (command.equals("jwworld", true)) {

            if (args.isEmpty() && sender !is Player) {

                sender.sendMessage(invalidConsoleArguments)
                sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                return true

            }

            if (args.size !in 0..1) {

                sender.sendMessage(invalidArguments)
                sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                return true

            }

            if (args.isEmpty()) {

                val world = (sender as Player).world

                sender.sendMessage("$prefix You are currently in ${world.name}.")

                return true

            }

            if (!sender.hasPermission("justworlds.command.jwworld.other")) {

                sender.sendMessage(otherPermission)
                sender.sendMessage("$error justworlds.command.jwworld.other")
                return true

            }

            val player = svr.getPlayer(args[0])

            if (player == null) {

                sender.sendMessage(unknownPlayer)
                return true

            }

            val world = player.world

            sender.sendMessage("$prefix Player ${player.name} is in ${world.name}.")

            return true

        }

        // jwlist [page]
        if (command.equals("jwlist", true)) {

            if (args.size !in 0..1) {

                sender.sendMessage(invalidArguments)
                sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                return true

            }

            val worlds = mutableMapOf<String, String>()

            findWorlds().forEach {

                worlds[it.name] = worldType(it)

            }

            val worldNames = worlds.keys.toList()

            var page = 1
            val lastPage = ((Math.ceil(worldNames.size / 8.0) * 8) / 8).toInt()

            if (args.size == 1) {

                if (args[0].toIntOrNull() == null) {

                    sender.sendMessage("$invalidPage 1-$lastPage")
                    return true

                }

                page = args[0].toInt()

            }

            if (page > lastPage || page == 0) {

                sender.sendMessage("$invalidPage 1-$lastPage")
                return true

            }

            val firstIndex = 8 * (page - 1)
            val secondIndex = if (page != lastPage) page * 8 else worldNames.size

            val pageWorlds = worldNames.subList(firstIndex, secondIndex)

            sender.sendMessage("$prefix List of worlds:")

            pageWorlds.forEach {

                val type = worlds[it]

                val color = when (type) {

                    "migrated" -> ChatColor.GREEN
                    "old" -> ChatColor.GOLD
                    "unmigrated" -> ChatColor.RED
                    "unloaded" -> ChatColor.YELLOW

                    else -> ChatColor.RED

                }

                sender.sendMessage("${ChatColor.DARK_AQUA}$it ${ChatColor.GRAY}- $color${type!!.capitalize()}")

            }

            sender.sendMessage("$prefix You are viewing page $page of $lastPage.")

            return true

        }

        // jwdelete <world>
        if (command.equals("jwdelete", true)) {

            if (args.size != 1) {

                sender.sendMessage(invalidArguments)
                sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                return true

            }

            val world = svr.getWorld(args[0])

            if (world == null) {

                sender.sendMessage(unknownWorld)
                return true

            }

            world.players.forEach {

                it.teleport(svr.worlds[0].spawnLocation)
                it.sendMessage("$prefix The world you're in is being deleted so you've been teleported to another.")

            }

            svr.unloadWorld(world, false)

            world.worldFolder.deleteRecursively()

            sender.sendMessage("$prefix Successfully deleted ${world.name}.")

            return true

        }

        // jwspawn [player]
        if (command.equals("jwspawn", true)) {

            if (args.size != 1 && sender !is Player) {

                sender.sendMessage(invalidConsoleArguments)
                sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                return true

            }

            if (args.size !in 0..1) {

                sender.sendMessage(invalidArguments)
                sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                return true

            }

            lateinit var player: Player

            if (args.size == 1) {

                if (!sender.hasPermission("justworlds.command.jwspawn.other")) {

                    sender.sendMessage(otherPermission)
                    sender.sendMessage("$error justworlds.command.jwspawn.other")
                    return true

                }

                if (svr.getPlayer(args[0]) == null) {

                    sender.sendMessage(unknownPlayer)
                    return true

                }

                player = svr.getPlayer(args[0])

            } else { player = sender as Player }

            val spawnLocation = player.world.spawnLocation

            player.teleport(spawnLocation)

            if (args.size == 1) { sender.sendMessage("$prefix You've been teleported to the world spawn.") }

            else {

                sender.sendMessage("$prefix You teleported ${player.name} to the world spawn.")
                player.sendMessage("$prefix You've been teleported to the world spawn by ${player.name}.")

            }

            return true

        }

        // jwsetspawn
        if (command.equals("jwsetspawn", true)) {

            if (sender !is Player) {

                sender.sendMessage(invalidConsoleArguments)
                sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                return true

            }

            if (args.isNotEmpty()) {

                sender.sendMessage(invalidArguments)
                sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                return true

            }

            val world = sender.world
            val jwFile = File("${world.worldFolder}${sep}justworlds.yml")

            if (!jwFile.exists()) {

                sender.sendMessage(notMigrated)
                return true

            }

            val location = sender.location

            val jwConfig = YamlConfiguration.loadConfiguration(jwFile)

            jwConfig.set("world.spawn.x", location.x)
            jwConfig.set("world.spawn.y", location.y)
            jwConfig.set("world.spawn.z", location.z)

            jwConfig.save(jwFile)

            world.spawnLocation = location

            sender.sendMessage("$prefix Successfully set the spawn for ${world.name}.")

            return true

        }

        // jwreload
        if (command.equals("jwreload", true)) {

            if (args.isNotEmpty()) {

                sender.sendMessage(invalidArguments)
                sender.sendMessage("$error ${cmd.usage.replace("<command>", label, true)}")
                return true

            }

            sender.sendMessage("$prefix Reloading JustWorlds...")

            pm.disablePlugin(this)
            pm.enablePlugin(this)

            reloadConfig()

            sender.sendMessage("$prefix Successfully reloaded JustWorlds.")

            return true

        }

        return true

    }

    @EventHandler
    fun onWorldInit(e: WorldInitEvent) {

        val world = e.world

        world.keepSpawnInMemory = false

        val x = world.spawnLocation.x.toInt()
        val z = world.spawnLocation.z.toInt()

        e.world.spawnLocation = Location(world, x.toDouble(), world.getHighestBlockYAt(x, z).toDouble(), z.toDouble())

    }

    @EventHandler
    fun onPlayerPortal(e: PlayerPortalEvent) {

        if (e.cause !in listOf(TeleportCause.NETHER_PORTAL, TeleportCause.END_PORTAL)) return

        val fromEnv = e.player.location.world.environment
        val toEnv = when (setOf(fromEnv, e.cause)) {

            setOf(World.Environment.NORMAL, TeleportCause.NETHER_PORTAL) -> World.Environment.NETHER
            setOf(World.Environment.NORMAL, TeleportCause.END_PORTAL) -> World.Environment.THE_END
            setOf(World.Environment.NETHER, TeleportCause.NETHER_PORTAL) -> World.Environment.NORMAL
            setOf(World.Environment.THE_END, TeleportCause.END_PORTAL) -> World.Environment.NORMAL

            else -> World.Environment.NORMAL

        }

        e.useTravelAgent(false)

        val fromName = e.player.location.world.name
        var toName = when (setOf(fromEnv, toEnv)) {

            setOf(World.Environment.NETHER, World.Environment.NORMAL) -> fromName.replace("_nether", "")
            setOf(World.Environment.THE_END, World.Environment.NORMAL) -> fromName.replace("_the_end", "")

            else -> "${fromName}_${toEnv.toString().toLowerCase()}"

        }

        if (fromName == toName) toName = "${fromName}_normal"

        var world = svr.getWorld(toName)

        if (world == null) {

            val wc = WorldCreator(toName)

            wc.environment(toEnv)

            wc.createWorld()

            world = svr.getWorld(toName)

            val jwFile = File("$root$sep${world.name}${sep}justworlds.yml")
            val jwConfig = YamlConfiguration.loadConfiguration(jwFile)

            jwConfig.set("world.name", name)
            jwConfig.set("world.environment", toEnv.toString())
            jwConfig.set("world.generator", "null")
            jwConfig.set("world.seed", world.seed)
            jwConfig.set("world.type", "NORMAL")
            jwConfig.set("world.spawn.x", world.spawnLocation.x)
            jwConfig.set("world.spawn.y", world.spawnLocation.y)
            jwConfig.set("world.spawn.z", world.spawnLocation.z)

            jwConfig.save(jwFile)

        }

        val newTo = e.to

        newTo.world = world

        e.to = newTo

    }

    private fun findWorlds(): List<File> {

        val directories = File(root).listFiles().filter { it.isDirectory }
        return directories.filter { File("$it${sep}level.dat").exists() }

    }

    private fun worldType(world: File, unloaded: Boolean = true): String {

        return when {
            File("$world${sep}justworlds.yml").exists() -> {
                if ((svr.getWorld(world.name) != null) || !unloaded) "migrated"
                else "unloaded"
            }

            File("$world${sep}world.jw").exists() -> {
                if ((svr.getWorld(world.name) != null) || !unloaded) "old"
                else "unloaded"
            }
            else -> {
                if ((svr.getWorld(world.name) != null) || !unloaded) "unmigrated"
                else "unloaded"
            }
        }

    }

}