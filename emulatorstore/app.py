#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask, render_template

app = Flask(__name__)
app.config.from_object(__name__)


@app.route('/')
def index():
    return render_template('index.html', section_title='A Titled Section')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
