#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask
from flask import render_template
from flask import request
from flask import redirect
from flask import url_for
from flask import flash
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import TextAreaField
from wtforms import SubmitField
from wtforms.validators import Email
from wtforms.validators import InputRequired
from flask_mail import Mail
from flask_mail import Message

app = Flask(__name__)
app.config.from_object('config')
mail = Mail(app)

class contactForm(FlaskForm):
    userName = StringField(u'Name:', [InputRequired()])
    userEmail = StringField(u'Email:', [InputRequired(), Email()])
    userSubject = StringField(u'Subject:', [InputRequired()])
    userMessage = TextAreaField(u'Message:', [InputRequired()])
    submitForm = SubmitField(u'Submit')

@app.route('/')
def index():
	return render_template('index.html', section_title='Home')

@app.route('/ios')
def ios():
    return render_template('ios.html', section_title='iOS')

@app.route('/ios/error')
def iosonly():
    return render_template('iosonly.html', section_title='iOS Only')

@app.route('/contact', methods=['GET', 'POST'])
def contact():
    form = contactForm()
    
    if request.method == 'POST':
        if form.validate():
            try:
                msg = Message(form.userSubject.data, sender=(form.userName.data, form.userEmail.data), recipients=["lpc@lukepchambers.net"], body=form.userMessage.data)
                msg.html = msg.body
                mail.send(msg)
                flash("Your message has been sent and I should reply within 48 hours.", "success")
            except:
                flash("Something went wrong when sending your message.", "danger")
            return redirect(url_for('index'))
        else:
            flash("Please enter a valid email.", "danger")
        
    return render_template('contact.html', form=form)

@app.route('/minecraft')
def minecraft():
    return render_template('minecraft.html', section_title='Minecraft')

@app.route('/about')
def about():
    return render_template('about.html', section_title='About')

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")