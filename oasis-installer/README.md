# oasis-installer
Install all oasis files easily
##Requirements
###Python 2
Well, duh, this script AND oasis were written in Python 2 so you need it!
####Note
You can NOT use Python 3 for either this script or oasis. The recommended version of Python is 2.7.10.
###Pip
Pip is a package manager for Python which we use to install an essential package to run oasis-installer.
###GitPython
Once you have Pip, you need to use the command `pip install GitPython` to install GitPython.
##Usage
To use oasis-installer, you need to first navigate to the oasis-installer directory.
`cd ~/path/to/oasis-installer/`
Next, verify you have the correct version of Python.
`python --version`
If you see "Python 2.7.10", you are good to go. Next, you need to run installer.py.
`python installer.py`
You will then be prompted to choose a destination for oasis to be installed to. The directory you choose **MUST** be empty.
`/users/yourname/path/to/oasis/`
Then, wait. If everything goes well, you will be displayed with a message telling you it was successfully installed to the designated directory. Enjoy oasis!
