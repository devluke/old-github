from __future__ import print_function
import os
import sys

try:
    import git
except ImportError:
    os.system("clear")
    print("Error: GitPython has not been installed. Install pip and type \"pip install GitPython\".")
    sys.exit(1)

os.system("clear")
path = raw_input("Install oasis to where?: ")
try:
    git.Git().clone("https://github.com/TheLukeGuy/oasis-launcher.git", path)
except:
    print("Error: Directory is not empty.")
    sys.exit(1)
os.system("clear")
print("Installed oasis successfully")
