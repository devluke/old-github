# get-mcserver
Easily download the vanilla Minecraft server .jar file.
## Usage
It's simple, just type in the console `cd path/to/get-mcserver`, then `python get-mcserver.py [server version]`.
