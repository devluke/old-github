from __future__ import print_function
import sys
import urllib
import os

try:
    version = sys.argv[1]
except IndexError:
    print("Please specify a version")
    sys.exit(1)

try:
    os.makedirs("downloaded/{}".format(version))
    urllib.urlretrieve("https://s3.amazonaws.com/Minecraft.Download/versions/{0}/minecraft_server.{0}.jar".format(version), "downloaded/{0}/minecraft_server.{0}.jar".format(version))
except Exception as e:
    print("An error occured while attempting to download minecraft_server.{}.jar".format(version))
    print("Error:", e)
    sys.exit(1)

print("Successfully downloaded minecraft_server.{}.jar".format(version))
