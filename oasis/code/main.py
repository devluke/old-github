#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division
from fractions import Fraction
from getpass import getpass
import sys
import math
import datetime
import os
import random
import urllib2
import urllib
import subprocess
import webbrowser
import time
import datetime
import threading
import readline
import base64
import shutil

#--------------------------------------------------#

# oasis configuration for hard-coded variables

# Play the animation when oasis is started?
# Default: True
ANIMATION = True

# In seconds, what speed should the animation be?
# Default: 0.025
SPEED = 0.025

# Should we show the development warning on start?
# Default: True for beta versions, False for releases.
DEVELOPMENT = True

# What is the current version of oasis?
# Default: Depends on version.
VERSION = "5.0-b3"

# What is the oasis codename for the current version?
# Default: Depends on version.
CODENAME = "Nitro"

# What is the date this oasis version was released?
# Default: Depends on version.
RELEASE = "1/7/18"

#--------------------------------------------------#


#--------------------------------------------------#

# Hard-coded variable declarations
# Do NOT change these values

FIRST_BOOT_FILE = 'data/extra/first_boot.txt'
NAME_FILE = 'data/profile/name.txt'
PASSWORD_FILE = 'data/profile/password.txt'
GUEST_MESSAGE_FILE = 'data/settings/guest_message.txt'
ALIAS_FILE = 'data/settings/aliases.txt'

DATA_FILE = "data.json"

TARGET_URL = 'http://tinyurl.com/oasis-update-check'

now = datetime.datetime.now()
year = now.year

stop_music_waves = 1

current_version = "oasis {} ({})".format(VERSION, RELEASE)

#--------------------------------------------------#


#--------------------------------------------------#

# Miscellaneous setups
# DO NOT TOUCH THIS, it's important code

readline.parse_and_bind('set editing-mode vi')

data = open(DATA_FILE, "w+")

def clear():
	os.system("clear")

def read_data():
	time.sleep(1)
	clear()
	print("Reading data...")
	print("Reading from name file...")
	with open(NAME_FILE, "r") as nf:
		name = ''.join(nf.readlines())
	time.sleep(0.01)
	clear()
	print("Reading data...")
	print("Reading from password file...")
	with open(PASSWORD_FILE, "r") as pf:
		password = ''.join(pf.readlines())
		password = base64.b64decode(password)
	time.sleep(0.01)
	clear()
	print("Reading data...")
	print("Reading from guest message file...")
	with open(GUEST_MESSAGE_FILE, "r") as gmf:
		guest_message = ''.join(gmf.readlines())
	time.sleep(0.01)
	clear()
	print("Reading data...")
	print("Reading from alias file...")
	with open(ALIAS_FILE, "r") as af:
		aliases = af.readlines()
		time.sleep(0.01)
		print("Setting up aliases...")
		alias = {}
		for i in aliases:
			current_alias = i.find(":")
			current_alias = i[:current_alias]
			current_command = i.find(":") + 1
			current_command = i[current_command:]
			alias[current_alias] = current_command
		time.sleep(0.01)
		clear()
		print("Reading data...")
		time.sleep(1)

	return (name, password, guest_message, alias)

def do_animation():
	print("(                              )")
	time.sleep(SPEED)
	clear()
	print(" (                            )")
	time.sleep(SPEED)
	clear()
	print("  (                          )")
	time.sleep(SPEED)
	clear()
	print("   (                        )")
	time.sleep(SPEED)
	print("    (                      )")
	time.sleep(SPEED)
	clear()
	print("     (                    )")
	time.sleep(SPEED)
	clear()
	print("      (                  )")
	time.sleep(SPEED)
	clear()
	print("       (                )")
	time.sleep(SPEED)
	clear()
	print("        (              )")
	time.sleep(SPEED)
	clear()
	print("         (            )")
	time.sleep(SPEED)
	clear()
	print("          (          )")
	time.sleep(SPEED)
	clear()
	print("           (        )")
	time.sleep(SPEED)
	clear()
	print("            (      )")
	time.sleep(SPEED)
	clear()
	print("             (    )")
	time.sleep(SPEED)
	clear()
	print("              (  )")
	time.sleep(SPEED)
	clear()
	print("               ()")
	time.sleep(SPEED)
	clear()
	print("               O")
	time.sleep(SPEED)
	clear()
	print("             O")
	time.sleep(SPEED)
	clear()
	print("           O")
	time.sleep(SPEED)
	clear()
	print("         O")
	time.sleep(SPEED)
	clear()
	print("       O")
	time.sleep(SPEED)
	clear()
	print("     o")
	time.sleep(SPEED)
	clear()
	print("   o")
	time.sleep(SPEED)
	clear()
	print(" o")
	time.sleep(SPEED)
	clear()
	print("o")
	time.sleep(SPEED)
	clear()
	print("oa")
	time.sleep(SPEED)
	clear()
	print("oas")
	time.sleep(SPEED)
	clear()
	print("oasi")
	time.sleep(SPEED)
	clear()
	print("oasis")
	time.sleep(3)
	clear()
	print("oasi")
	time.sleep(SPEED)
	clear()
	time.sleep(SPEED)
	clear()
	print("oas")
	time.sleep(SPEED)
	clear()
	print("oa")
	time.sleep(SPEED)
	clear()
	print("o")
	time.sleep(SPEED)
	clear()

#--------------------------------------------------#

try:
	import newcrypt
	clear()
	print("Please fully redownload oasis to upgrade to version 5.0 Nitro or above.")
	print("ONLY if you just downloaded this, try removing newcrypt.py and newcrypt.pyc.")
	print("If you just updated, don't remove ANY files. The filesystem needs to be migrated.")
	sys.exit(1)
except ImportError:
	clear()

if (DEVELOPMENT):
	print("Warning - this is a development version of oasis and may contain bugs 3")
	time.sleep(1)
	clear()
	print("Warning - this is a development version of oasis and may contain bugs 2")
	time.sleep(1)
	clear()
	print("Warning - this is a development version of oasis and may contain bugs 1")
	time.sleep(1)
clear()

if not os.path.isfile(DATA_FILE):
	print("Welcome to oasis. Press enter to begin setup.")
	raw_input("(press enter) ")
	clear()

	time.sleep(3)
	open(FIRST_BOOT_FILE, 'a').close()
	set_name = raw_input("Username - ")
	nf = open(NAME_FILE, 'w')
	nf.write(set_name)
	nf.close()
	clear()

	while True:
		set_password = getpass("Password - ")
		clear()
		password_confirm = getpass("Confirm password - ")
		if password_confirm == set_password:
			clear()
			break
		else:
			clear()
			print("Passwords do not match...")
			time.sleep(3)
		clear()
	set_password = base64.b64encode(set_password)
	pf = open(PASSWORD_FILE, 'w')
	pf.write(set_password)
	pf.close()
	clear()

	print("Successfully setup oasis.")
	print("Reading data...")
	name, password, guest_message, alias = read_data()
	print("oasis {} - type \"cmds\" for a list of commands".format(VERSION))
	print("Welcome to oasis, {}!".format(name))
	mode = name
else:
	with open(FIRST_BOOT_FILE, "r") as fbf:
		testhardreset = fbf.read()
		if testhardreset == "hardreset":
			clear()
			print("To finish, type into the console \"python reset.py\"")
			sys.exit(1)
	print("Reading data...")
	name, password, guest_message, alias = read_data()
	print("Users - {}, guest".format(name))
	mode = raw_input("Name - ")
	if mode == "guest":
		clear()
		print(guest_message)
		print("oasis {} - type \"cmds\" for a list of commands".format(VERSION))

	elif mode == name:
		tries = 0
		while tries < 3:
			try_password = getpass("Password for {} - ".format(name))
			if try_password == password:
				clear()
				print("oasis {} - type \"cmds\" for a list of commands".format(VERSION))
				print('Welcome back, {}!'.format(name))
				break
			else:
				print("Incorrect password")
				tries += 1
		else:
			print("Multiple failed attempts")
			sys.exit(1)

	else:
		clear()
		print("Error - Unknown user")
		sys.exit(1)

if not mode == "guest":
	for i in alias:
		commands.append(i)

readline.clear_history()

runalias = False
runscript = False

while True:
	try:
		print("\n")
		if runalias:
			runalias = False
		else:
			command = raw_input("> ")

		if command == "help":
			print("For a list of commands, type \"cmds\"")

		elif command == "cmds":
			if mode == "guest":
				print("Available commands:")
				print("help - get oasis-related help")
				print("cmds - show this list of commands")
				print("calculator - a basic calculator")
				print("clear - clears console")
				print("quit - quits oasis")
				print("about - about your copy of oasis")
				print("update - checks for oasis updates")
				print("xmas - {} Christmas countdown".format(year))
				print("convert - simple math conversions")
				print("piglatin - a simple pig latin translator")
				print("stopwatch - a simple stopwatch")
				print("rps - rock, paper, scissors")
				print("version - current oasis version")
				print("guess - number guessing game")
				print("restart - restarts oasis")
				print("scramble - cube scrambler")
				raise KeyboardInterrupt

			print("Available commands:")
			print("help - get oasis-related help")
			print("cmds - show this list of commands")
			print("calculator - a basic calculator")
			print("clear - clears console")
			print("quit - quits oasis")
			print("text - save text to a local file")
			print("about - about oasis")
			print("update - checks for oasis updates")
			print("music - plays music you upload")
			print("lock - locks oasis until you enter your password")
			print("xmas - {} Christmas countdown".format(year))
			print("convert - simple math conversions")
			print("piglatin - a simple pig latin translator")
			print("books - read downloaded e-books")
			print("downloader - download files required by certain programs")
			print("settings - configure oasis to your liking")
			print("stopwatch - a simple stopwatch")
			print("rps - rock, paper, scissors")
			print("hardreset - reset every file")
			print("run [file name] - run an oasis script")
			print("updater - update oasis")
			print("version - current oasis version")
			print("guess - number guessing game")
			print("restart - restarts oasis")
			print("scramble - cube scrambler")

			for i in alias:
				if not i.strip() == "":
					print("{} - alias for \"{}\"".format(i.strip(), alias[i.strip()].strip()))

		elif command == "clear":
			clear()
			print("Console cleared")

		elif command == "quit":
			clear()
			if mode == "guest":
				print("Farewell until we meet again!")
				sys.exit(0)
			print("Farewell until we meet again, {}!".format(name))
			sys.exit(0)

		elif command == "text":
			if mode == "guest":
				raise KeyboardInterrupt
			print("Files are saved to \"files\" folder")
			print("Put {0} to get a new line")
			text = raw_input("Text > ")
			text = text.format("\n")
			while True:
				editorchoice = raw_input("Save / discard file - ")
				if editorchoice == "save":
					save_as = raw_input("Save file as - ")
					TEXT_FILE = 'files/{}'.format(save_as)
					tf = open(TEXT_FILE, 'w')
					tf.write(text)
					tf.close()
					print("Saved successfully")
					break
				elif editorchoice == "discard":
					break
				else:
					print("Error - Invalid answer")
					time.sleep(3)

		elif command == "about-legacy":
			print("oasis {} running on {}.".format(VERSION, os.uname()[1]))
			print("written in Python")
			print("coded in Atom, TextWrangler, and Xcode")

		elif command == "update":
			try:
				for line in urllib2.urlopen(TARGET_URL):
					version = line
			except:
				print("Error - Could not connect to {}".format(TARGET_URL))
				raise KeyboardInterrupt
			if not version == current_version:
				print("Stable release available - {}".format(version))
			else:
				print("No updates available")

		elif command == "music":
			if mode == "guest":
				raise KeyboardInterrupt
			stop_music_waves = 0
			def music_waves():
				while stop_music_waves == 0:
					print(".")
					print("\n")
					print("Press CTRL+C to exit.")
					time.sleep(0.1)
					clear()
					print(".:")
					print("\n")
					print("Press CTRL+C to exit.")
					time.sleep(0.1)
					clear()
					print(".:.")
					print("\n")
					print("Press CTRL+C to exit.")
					time.sleep(0.1)
					clear()
					print(".:")
					print("\n")
					print("Press CTRL+C to exit.")
					time.sleep(0.1)
					clear()
					print(".")
					print("\n")
					print("Press CTRL+C to exit.")
					time.sleep(0.1)
					clear()
					print("")
					print("\n")
					print("Press CTRL+C to exit.")
					time.sleep(0.1)
					clear()
				print("CTRL+C one more time!")
			print("Store music in the \"files\" folder")
			MUSIC_FILE = raw_input("Music file - ")
			clear()
			threading.Thread(target = music_waves).start()
			afplay.afplay("files/{}".format(MUSIC_FILE))
			raise KeyboardInterrupt

		elif command == "lock":
			if mode == "guest":
				raise KeyboardInterrupt
			clear()
			tries = 0
			while tries < 3:
				unlock = getpass("Password for {} - ".format(name))
				if unlock == password:
					clear()
					print("Welcome back, {}!".format(name))
					break
				else:
					tries += 1
			else:
				clear()
				print("Incorrect password 3 times")
				print("Exiting...")
				sys.exit(0)

		elif command == "xmas":
			months = 12 - now.month
			days = 25 - now.day
			if months == 1:
				month_format = "month"
			else:
				month_format = "months"
			if days == 1:
				day_format = "day"
			else:
				day_format = "days"
			print("Christmas {} is in only {} {} and {} {}!".format(year, months, month_format, days, day_format))

		elif command == "convert":
			print("pyCalc convert v0.2")
			print("---")
			print("Convert options - decimal, fraction, percent")
			convert1 = raw_input("Convert what - ")
			if not convert1 in ["decimal", "fraction", "percent"]:
				print("Error - Invalid input")
				raise KeyboardInterrupt
			convert2 = raw_input("To what - ")
			if not convert2 in ["decimal", "fraction", "percent"]:
				print("Error - Invalid input")
				raise KeyboardInterrupt
			if convert1 == "fraction" and convert2 == "percent":
				numerator = raw_input("Numerator - ")
				denominator = raw_input("Denominator - ")
				answer = str(int(numerator) / int(denominator) * 100)
				print("{}/{} converted to a percent is {}".format(numerator, denominator, answer))
			elif convert1 == "decimal" and convert2 == "fraction":
				decimal = raw_input("Decimal - ")
				answer = Fraction(float(decimal))
				print("{} converted to a fraction is {}".format(decimal, answer))
			elif convert1 == "fraction" and convert2 == "decimal":
				numerator = raw_input("Numerator - ")
				denominator = raw_input("Denominator - ")
				answer = str(int(numerator) / int(denominator))
				print("{}/{} converted to decimal is {}".format(numerator, denominator, answer))
			else:
				print("error - this conversion is work in progress")
				raise KeyboardInterrupt

		elif command == "animation":
			do_animation()
			print("Played animation")

		elif command == "piglatin":
			tofrom = raw_input("Translate (to/from) pig latin - ")
			if tofrom not in ["to", "from"]:
				print("Error - Invalid input")
				raise KeyboardInterrupt
			if tofrom == "to":
				firstpl = raw_input("Convert what to pig latin - ")
				secondpl = firstpl[0]
				thirdpl = firstpl[1:]
				answer = "{}{}ay".format(thirdpl, secondpl)
				print("{} in pig latin is {}".format(firstpl, answer))
			elif tofrom == "from":
				firstpl = raw_input("Convert what from pig latin - ")
				getlettersbefore = len(firstpl) - 3
				secondpl = firstpl[:getlettersbefore]
				thirdpl = firstpl[getlettersbefore]
				answer = "{}{}".format(thirdpl, secondpl)
				print("{} from pig latin is {}".format(firstpl, answer))

		elif command == "books":
			if mode == "guest":
				raise KeyboardInterrupt
			print("Download books with the \"downloader\" command")
			bauthor = raw_input("Book author - ")
			btitle = raw_input("Book title - ")
			BOOK_FILE = "{}-{}.oasis".format(btitle, bauthor)
			try:
				with open("files/{}".format(BOOK_FILE), "r") as bf:
					book = bf.read()
			except IOError:
				try:
					open("files/{}-{}.txt".format(btitle, bauthor), "r").close()
					print("Error - Old book type (try changing .txt to .oasis)")
					raise KeyboardInterrupt
				except IOError:
					print("Error - Unknown file")
					raise KeyboardInterrupt
			clear()
			print("Entering book mode. To exit, press enter or return")
			print("\n")
			print(book)
			raw_input()
			clear()
			print("Exiting book mode")

		elif command == "downloader":
			if mode == "guest":
				raise KeyboardInterrupt
			else:
				dltype = raw_input("Download type (book, script) - ")
				if not dltype in ["book", "script"]:
					print("Invalid input")
					raise KeyboardInterrupt
				if dltype == "book":
					TARGET_URL = raw_input("Download URL - ")
					book_download = []
					try:
						for line in urllib2.urlopen(TARGET_URL):
							book_download.append(line)
					except:
						print("Error - Invalid URL")
						raise KeyboardInterrupt
					book_title = book_download[0]
					book_author = book_download[1]
					book_download = book_download[2:]
					NEW_BOOK_FILE = "{}-{}.txt".format(book_title.strip(), book_author.strip())
					book_download_length = len(book_download)
					book_download_str = "".join(book_download)
					open("files/{}".format(NEW_BOOK_FILE), 'a').close()
					with open("files/{}".format(NEW_BOOK_FILE), 'w') as nbf:
						nbf.write(book_download_str)
					print("Successfully downloaded")
				elif dltype == "script":
					TARGET_URL = raw_input("Download URL - ")
					scriptdl = []
					try:
						for line in urllib2.urlopen(TARGET_URL):
							scriptdl.append(line)
					except:
						print("Error - Invalid URL")
						raise KeyboardInterrupt
					NEW_SCRIPT_FILE = "{}.oasis".format(raw_input("Script name - "))
					script_download_str = "".join(scriptdl)
					open("files/{}".format(NEW_SCRIPT_FILE), 'a').close()
					with open("files/{}".format(NEW_SCRIPT_FILE), 'w') as nbf:
						nbf.write(script_download_str)
					print("Success - Download")

		elif command == "settings":
			if mode == "guest":
				raise KeyboardInterrupt
			print("Settings:")
			print("setpass - change your password to oasis")
			print("guestmessage - change the guest login message")
			print("setalias - add a command alias")
			print("\n")
			setting = raw_input("setting - ")
			if setting == "setpass":
				clear()
				if getpass("Old password - ") == password:
					clear()
					while True:
						set_password = getpass("New password - ")
						clear()
						password_confirm = getpass("Confirm new password - ")
						if password_confirm == set_password:
							clear()
							break
						else:
							clear()
							print("Error - Passwords do not match")
							time.sleep(3)
						clear()
					set_password = base64.b64encode(set_password)
					pf = open(PASSWORD_FILE, 'w')
					pf.write(set_password)
					pf.close()
					clear()
					print("Successfully changed password")
					print("For changes to take effect, you must restart oasis")
					while True:
						restart = raw_input("restart now? (y/n) ")
						if restart == "y":
							clear()
							print("To start, \"python main.py\"")
							sys.exit(0)
						elif restart == "n":
							break
						else:
							print("Error - Invalid input")
							time.sleep(1)
							print("\n")

				else:
					print("Error - Incorrect password")
					raise KeyboardInterrupt

			elif setting == "guestmessage":
				with open(GUEST_MESSAGE_FILE, "w") as gmf:
					gmf.write(raw_input("New message - "))
				print("Successfully changed guest password")

			elif setting == "setalias":
				get_aliases = []
				with open(ALIAS_FILE, "r") as af:
					get_aliases = af.readlines()
					af.close()
				with open(ALIAS_FILE, "w") as af:
					get_aliases.append("{}:{}".format(raw_input("Alias: "), raw_input("Command: ")))
					af.write("\n".join(get_aliases))
				print("Successfully created alias")
				print("For changes to take effect, you must restart oasis")
				while True:
					restart = raw_input("Restart now? (y/n) ")
					if restart == "y":
						clear()
						print("To start, \"python main.py\"")
						sys.exit(0)
					elif restart == "n":
						break
					else:
						print("Error - Invalid input")
						time.sleep(1)
						print("\n")

			else:
				print("Error - Invalid setting")
				raise KeyboardInterrupt

		elif command == "":
			raise KeyboardInterrupt

		elif command in alias:
			if mode == "guest":
				raise KeyboardInterrupt
			runalias = True
			command = alias[command]
			command = command.strip()

		elif command == "stopwatch":
			clear()
			print("Stopwatch")
			print("\n")
			print("Use CTRL + C to stop stopwatch")
			raw_input("Press enter to begin")
			clear()
			hours = 0
			minutes = 0
			seconds = 0
			try:
				while True:
					time.sleep(1)
					if str(seconds) == "59":
						minutes += 1
						seconds = 0
					elif str(minutes) == "59":
						hours += 1
						minutes = 0
					else:
						seconds += 1
					clear()
					if hours < 10:
						printableh = "0{}".format(str(hours))
					else:
						printableh = str(hours)
					if minutes < 10:
						printablem = "0{}".format(str(minutes))
					else:
						printablem = str(minutes)
					if seconds < 10:
						printables = "0{}".format(str(seconds))
					else:
						printables = str(seconds)
					print("{}:{}:{}".format(printableh, printablem, printables))
			except KeyboardInterrupt:
				clear()
				print("{}:{}:{}".format(printableh, printablem, printables))

		elif command == "rps":
			print("Mode - (1) against computer or (2) multiplayer")
			rpsmode = raw_input()
			if rpsmode not in ["1", "2"]:
				print("Error - Invalid input")
				raise KeyboardInterrupt
			elif rpsmode == "1":
				while True:
					rps = raw_input("rock, paper, or scissors? ")
					if not rps in ["rock", "paper", "scissors"]:
						print("Error - Invalid input")
						pass
					else:
						print("\n")
						cpu = random.choice(["rock", "paper", "scissors"])
						print("Computer chooses {}".format(cpu))
						if rps == cpu:
							print("Tie game")
							print("\n")
						elif rps == "rock" and cpu == "paper":
							print("Computer wins")
							print("\n")
						elif rps == "paper" and cpu == "rock":
							print("Player wins")
							print("\n")
						elif rps == "paper" and cpu == "scissors":
							print("Computer wins")
							print("\n")
						elif rps == "scissors" and cpu == "paper":
							print("Player wins")
							print("\n")
						elif rps == "scissors" and cpu == "rock":
							print("Computer wins")
							print("\n")
						elif rps == "rock" and cpu == "scissors":
							print("Player wins")
							print("\n")
			else:
				while True:
					p1 = getpass("P1: rock, paper, or scissors? ")
					if not p1 in ["rock", "paper", "scissors"]:
						print("Error - Invalid input")
						pass
					else:
						p2 = getpass("P2: rock, paper, or scissors? ")
						if not p2 in ["rock", "paper", "scissors"]:
							print("Error - Invalid input")
							pass
						else:
							print("\n")
							print("P1 picked {}".format(p1))
							print("P2 picked {}".format(p2))
							if p1 == p2:
								print("Tie game")
								print("\n")
							elif p1 == "rock" and p2 == "paper":
								print("P2 wins")
								print("\n")
							elif p1 == "paper" and p2 == "rock":
								print("P1 wins")
								print("\n")
							elif p1 == "paper" and p2 == "scissors":
								print("P2 wins")
								print("\n")
							elif p1 == "scissors" and p2 == "paper":
								print("P1 wins")
								print("\n")
							elif p1 == "scissors" and p2 == "rock":
								print("P2 wins")
								print("\n")
							elif p1 == "rock" and p2 == "scissors":
								print("P1 wins")
								print("\n")

		elif command == "about":
			clear()
			time.sleep(2)
			print("o")
			time.sleep(0.1)
			clear()
			print("oa")
			time.sleep(0.1)
			clear()
			print(" as")
			time.sleep(0.1)
			clear()
			print("  si")
			time.sleep(0.1)
			clear()
			print("   is")
			time.sleep(0.1)
			clear()
			print("    s")
			clear()
			time.sleep(1)
			print("o")
			time.sleep(0.1)
			clear()
			print("oa")
			time.sleep(0.1)
			clear()
			print("oas")
			time.sleep(0.1)
			clear()
			print("oasi")
			time.sleep(0.1)
			clear()
			print("oasis")
			time.sleep(3)
			clear()
			print("oasis")
			time.sleep(2)
			print("version {}".format(VERSION))
			time.sleep(3)
			print("\n")
			print("a project by TheLukeGuy")
			time.sleep(2)
			print("formally known as tlgOS")
			time.sleep(2)
			print("started late 2015")
			time.sleep(2)
			print("coded in Atom, TextWrangler, and Xcode")
			time.sleep(2)
			print("coded on two different MacBook Pros and a Mac mini")
			time.sleep(3)
			print("\n")
			print("website: http://lukepchambers.net/oasis")
			time.sleep(2)
			print("GitHub: https://github.com/TheLukeGuy/oasis")
			print("\n")
			time.sleep(3)
			print("press enter to exit")
			raw_input()
			clear()
			raise KeyboardInterrupt

		elif command == "hardreset":
			if mode == "guest":
				raise KeyboardInterrupt
			confirm_reset = raw_input("Confirm hard reset (y/n) ")
			if confirm_reset == "n":
				raise KeyboardInterrupt
			elif not confirm_reset in ["y", "n"]:
				print("error - invalid answer")
				raise KeyboardInterrupt
			confirm_reset2 = raw_input("All files will be deleted forever, confirm? (y/n) ")
			if confirm_reset2 == "n":
				raise KeyboardInterrupt
			elif not confirm_reset2 in ["y", "n"]:
				print("Error - Invalid answer")
				raise KeyboardInterrupt
			print("Resetting...")
			os.system("rm -rf data/profile/*.txt")
			open(NAME_FILE, "a").close()
			open(PASSWORD_FILE, "a").close()
			os.system("rm -rf data/settings/*.txt")
			open(ALIAS_FILE, "a").close()
			open(GUEST_MESSAGE_FILE, "a").close()
			with open(GUEST_MESSAGE_FILE, "w") as gmf:
				gmf.write("Logged in as a guest")
			with open(FIRST_BOOT_FILE, "w") as fbf:
				fbf.write("hardreset")
			fbf.close()
			clear()
			print("To finish hard reset, type into the console \"python reset.py\"")
			sys.exit(0)

		elif command == "run":
			if runscript:
				script = scripttorun
				runscript = False
			else:
				clear()
				print("oasis script beta 0.5")
				script = raw_input("File location (files are stored in the files folder) - ")
			clear()
			if not os.path.isfile("files/{}".format(script)):
				print("Error - Invalid file")
				raise KeyboardInterrupt
			with open("files/{}".format(script), "r") as sf:
					script_list = sf.readlines()
			if not script[len(script) - 6:] == ".oasis":
				print("[Script] - Old script type (try changing .txt to .oasis)")
				raise KeyboardInterrupt
			clear()
			osanswer = "null"
			try:
				for i in script_list:
					if i[0:3] == "say":
						print(i[4:].format(osanswer))
					if i[0:4] == "wait":
						time.sleep(int(i[5:].format(osanswer)))
					if i[0:5] == "clear":
						clear()
					if i[0:5] == "enter":
						raw_input(i[6:].format(osanswer))
					if i[0:5] == "input":
						osanswer = raw_input(i[6:].format(osanswer))
					if i[0:4] == "math":
						osanswer = eval(i[5:].format(osanswer))
					if i[0:6] == "length":
						osanswer = len(i[7:])
			except Exception as e:
				print("\n")
				print("[Script] - Python error")
				continue
			print("\n")
			print("[Script] - End of script")

		elif command[0:3] == "run" and len(command) > 3:
			scripttorun = command[4:]
			runalias = True
			runscript = True
			command = "run"
			command = command.strip()

		elif command == "mcpi":
			try:
				from mcpi.minecraft import Minecraft
				mc = Minecraft.create()
			except:
				print("Error - Cannot find running MCPI session")
				raise KeyboardInterrupt
			print("Oh, cool, you have Minecraft: Pi edition running!")
			print("Here, have some TNT!")
			mc.postToChat("Here, have some TNT!")
			x, y, z = mc.player.getPos()
			mc.player.setPos(x, y+5, z-10)
			mc.setBlocks(x, y, z, x+5, y+5, z+5, 46, 1)

		elif command == "calculator":
			print("pyCalc3-wip")
			print("\n")
			while True:
				calc = raw_input("Expression - ")
				calc_list = list(calc)
				check_valid = []
				for i in calc_list:
					if i in ["+", "-", "*", "/"]:
						check_valid.append(i)
				if len(check_valid) >= 1:
					try:
						print(eval(calc))
					except SyntaxError:
						print("Error")
						pass
				elif calc == "":
					raise KeyboardInterrupt
				elif calc == "pi":
					print("3.141592653589793238462643")
				else:
					print("Error")
					raise KeyboardInterrupt
				print("\n")

		elif command == "updater":
			if mode == "guest":
				raise KeyboardInterrupt
			clear()
			print("Preparing for update...")
			print("Checking version...")
			print(VERSION)
			print("\n")
			print("oasis will update main.py from version {} to latest source code release.".format(VERSION))
			raw_input("Press enter to update, CTRL+C to cancel. ")
			clear()
			print("Updating oasis, please don't touch ANY key during this process.")
			print("\n")
			print("Deleting main.py...")
			os.remove("main.py")
			print("Downloading oasis as main.py...")
			urllib.urlretrieve("https://raw.githubusercontent.com/TheLukeGuy/oasis/master/main.py", "main.py")
			time.sleep(3)
			clear()
			print("Updated successfully. Restarting in 3...")
			time.sleep(1)
			clear()
			print("Updated successfully. Restarting in 2...")
			time.sleep(1)
			clear()
			print("Updated successfully. Restarting in 1...")
			time.sleep(1)
			clear()
			if mode == "guest":
				print("Farewell until we meet again!")
			else:
				print("Farewell until we meet again, {}!".format(name))
			time.sleep(3)
			clear()
			time.sleep(2)
			os.execv(sys.executable, ['python'] + sys.argv)

		elif command == "version":
			print(VERSION)

		elif command == "guess":
			# Begin configuration
			MIN_NUMBER = 1
			MAX_NUMBER = 100
			ATTEMPTS = 10
			# End configuration
			attempts = ATTEMPTS
			number = random.randint(MIN_NUMBER, MAX_NUMBER)
			print("Number - between {} and {}".format(MIN_NUMBER, MAX_NUMBER))
			print("Attempts - {}".format(ATTEMPTS))
			print("\n")
			while attempts > 0:
				attempts -= 1
				guess = raw_input("Guess {}/{} - ".format(ATTEMPTS - attempts, ATTEMPTS))
				try:
					if int(guess) > number:
						print("Too high")
					elif int(guess) < number:
						print("Too low")
					else:
						print("\n")
						print("Number guessed in {} attempts".format(ATTEMPTS - attempts))
						raise KeyboardInterrupt
				except ValueError:
					print("Error - Invalid input")
					continue
			print("\n")
			print("Game over, the number was {}".format(number))

		elif command == "guest":
			if not mode == "guest":
				print("False")
			else:
				print("True")

		elif command == "cmds":
			for i in commands:
				print(i)

		elif command == "restart":
			clear()
			if mode == "guest":
				print("Farewell until we meet again!")
			else:
				print("Farewell until we meet again, {}!".format(name))
			time.sleep(3)
			clear()
			time.sleep(2)
			os.execv(sys.executable, ['python'] + sys.argv)

		elif command == "scramble":
			notation = ["L", "R", "U", "D", "F", "B"]
			special = ["'", "2", ""]
			length = 30
			scramble_list = []
			while length > 0:
				scramble_list.append("{}{}".format(random.choice(notation), random.choice(special)))
				length -= 1
			scramble = " ".join(scramble_list)
			print(scramble)

		else:
			print("Invalid command - {} - type \"cmds\" for a list of commands".format(command))

	except KeyboardInterrupt:
		stop_music_waves = 1
		continue
