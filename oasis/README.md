# oasis
Welcome to oasis, my first data-saving operating system!
## Installing
Simply clone the repo!
## What is oasis?
oasis is basically an operating system built in Python. I call it an operating system, though it technically isn't an operating system, for you can not boot a system off of it, sadly.
### What can you do in it?
Well, there are a bunch of applications you can run and do stuff with, some are for productivity, others just for fun. You can look at the wiki page on applications & programs over at `https://github.com/TheLukeGuy/oasis/wiki/Applications-Programs` for the full list of applications.
##Requirements
###Python
Duh, you need Python because oasis was developed in Python.
##Opening/Resetting oasis
Open Terminal, type `cd [path to the oasis folder]`, then `python main.py`. To reset oasis and restart setup, make sure you are in the oasis directory, then type `python reset.py`.
