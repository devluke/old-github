# PLEASE READ!
This tool is broken as a used API changed it's usage and I really can't figure out what's wrong with my code using the new API version. I'm not going to be continuing development of this tool so it'll probably stay in it's same broken state forever... Sorry!

# scaryinternet
Find out where someone lives... Is this legal?
## Usage
Just click on the link listed under the description! It'll guide you into finding either your's or someone else's location. Scary, right?
## API
An API for developers is coming soon!
## Um...
I know, this is scary. You can protect yourself by getting a VPN (virtual private network) so that you can hide your IP address and trick websites into thinking you live somewhere you don't. This is highly recommended. You can find some free ones online.
## Q/A
#### How does this even work?
The main method works by grabbing your IP address, getting your latitude and longitude from your IP address, and getting your address from those coordinates.
#### Is this legal?
Completely! Although this may be scary, using these methods to get information about people is completely legal in the US and some other countries. *plz no sue meh, im only a child!*
#### What if I have a VPN?
If you have a VPN, the main methods won't work. VPNs (virtual private networks) hide your IP address and most methods require your IP address. You can alternatively enter your IP address manually if you know it if you really want this to work.
#### Why? Just why?
I don't know! Why not?
## Credits
#### [freegeoip.net](https://freegeoip.net) - Provides IP information

#### [OpenStreetMap](http://www.openstreetmap.org) - Provides address information
