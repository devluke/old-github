def initApp():
    # Add lines to run when the app is initialized below this comment

    # Which user type should be able to run this command: user or admin?
    appPerm = "admin"
    return appPerm

def runApp():
    import lib.newcrypt
    import getpass
    import os

    os.chdir("../users")
    setuser = "{}".format(raw_input("Choose username: "))
    open(setuser + ".json", "a").close()
    jsonfile = livejson.File("{}.json".format(setuser), pretty=True)
    jsonfile["user"] = setuser
    setname = raw_input("Full name: ")
    jsonfile["name"] = setname
    toencrypt = getpass.getpass("Choose password: ")
    encryptedpass = lib.newcrypt.encrypt("{}userpass".format(lib.newcrypt.encrypt(toencrypt)))
    jsonfile["password"] = encryptedpass
    while True:
        setgroup = raw_input("Set group (user/admin): ")
        if setgroup in ["user", "admin"]:
            jsonfile["group"] = setgroup
            break