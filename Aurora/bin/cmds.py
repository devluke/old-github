def initApp():
    # Add lines to run when the app is initialized below this comment

    # Which user type should be able to run this command: user or admin?
    appPerm = "user"
    return appPerm

def runApp():
    import glob

    commands = []

    for i in glob.glob("*.py"):
        if not i == "__init__.py":
            commands.append(i)


    print("List of programs:")
    for i in commands:
        print("{}. {}".format(commands.index(i) + 1, i[:-3]))