from __future__ import print_function
import getpass
import select
import glob
import sys
import os

booterVersion = "0.1"
auroraVersion = "0.1 alpha-pre1"
requiredLibraries = ["newcrypt"]
requiredPyPi = ["livejson", "readline"]

if __name__ == '__main__':
    print("Press enter within 5 seconds to boot, otherwise don't press anything for recovery mode.")
    i, o, e = select.select([sys.stdin], [], [], 5)
    print("\n")
    if not i:
        print("Entering recovery mode...")
        import recovery
        sys.exit(0)

print("Aurora booter v{}".format(booterVersion))
print("\n")
print("Loading and importing all libraries in lib directory...")
os.chdir("lib")
for i in glob.glob("*.py"):
    if not i == "__init__.py":
        print("Loading - {}".format(i))
        i = i[:-3]
        exec("import lib.{}".format(i))
        try:
            exec("lib.{}.onLoad()".format(i))
        except AttributeError:
            print("Error loading {} - onLoad() statement missing".format(i))
            exec("del lib.{}".format(format(i)))
            continue
        print("Loaded - {}".format(i))
print("\n")
print("Checking for required libraries...")
missingLibraries = []
for i in requiredLibraries:
    try:
        exec("lib.{}.onLoad()".format(i))
        print("Found - {}".format(i))
    except:
        print("Missing library - {}".format(i))
        missingLibraries.append(i)
        continue
    for i in requiredPyPi:
        try:
            exec("import {}".format(i))
            print("Found - {}".format(i))
        except:
            print("Missing library - {}".format(i))
            missingLibraries.append(i)
if len(missingLibraries) >= 1:
    print("\n")
    print("Missing libraries:")
    for i in missingLibraries:
        print(i)
    print("\n")
    print("Could not boot Aurora, {} essential libraries (listed above) missing. Install these libraries and try again.".format(len(missingLibraries)))
    sys.exit(1)
print("\n")
print("Preparing to read users...")
users = []
names = []
passwords = []
groups = []
print("Reading users...")
os.chdir("../users")
for i in glob.glob("*.json"):
    jsonfile = livejson.File(i, pretty=True)
    users.append(jsonfile["user"])
    names.append(jsonfile["name"])
    passwords.append(jsonfile["password"])
    groups.append(jsonfile["group"])
if len(users) <= 0:
    print("No users, creating root user")
    print("\n")
    open("root.json", "a").close()
    jsonfile = livejson.File("root.json", pretty=True)
    jsonfile["user"] = "root"
    jsonfile["name"] = "root"
    toencrypt = getpass.getpass("Choose root password: ")
    encryptedpass = lib.newcrypt.encrypt("{}userpass".format(lib.newcrypt.encrypt(toencrypt)))
    jsonfile["password"] = encryptedpass
    jsonfile["group"] = "admin"
    print("\n")
    print("Please restart Aurora to finish setup.")
    sys.exit(0)
while True:
    print("\n")
    user = raw_input("Username: ")
    attemptpass = getpass.getpass("Password: ")
    try:
        userid = users.index(user)
    except:
        print("Incorrect username or password")
        continue
    suser = users[userid]
    sname = names[userid]
    spassword = lib.newcrypt.decrypt(lib.newcrypt.decrypt(passwords[userid])[:-8])
    sgroup = groups[userid]
    if attemptpass == spassword:
        break
    else:
        print("Incorrect username or password")

print("Logged in as {}, type \"cmds\" for a list of programs".format(sname))
while True:
    print("\n")
    os.chdir("../bin")
    try:
        command = raw_input("{}$ ".format(user))
        commandFirstWord = command.split()[0]
        commandArgs = command.split()[1:]
        commandArgs = ', '.join(commandArgs)
        exec("from bin.{} import *".format(commandFirstWord))
        appPerm = initApp()
        if appPerm == "admin":
            if sgroup == "admin":
                print("runApp({})".format(commandArgs))
                exec("runApp({})".format(commandArgs))
            else:
                print("You don't have permission to run this command")
                continue
        else:
            runApp()
    except Exception as e:
        print("Unknown error while running command:")
        print(e)