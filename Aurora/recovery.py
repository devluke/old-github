import shutil
import os

while True:
    recoverMode = raw_input("What problem are you having? (error, other) ")
    if recoverMode in ["error", "other"]:
        break

print("\n")
if recoverMode == "error":
    print("Aurora will run with error detection on, use it normally until the error occurs.")
    raw_input("Press enter to start: ")
    print("\n")
    try:
        import main
    except Exception as e:
        print("\n")
        while True:
            isError = raw_input("Does the error say \"{}: {}\"? (y/n) ".format(type(e).__name__, e))
            if isError in ["y", "n"]:
                break
        print("\n")
        if isError == "n":
            print("If this isn't your error:")
            print("Create an issue on the GitHub page or check the wiki on GitHub.")
        else:
            e = str(e)
            if e in ["'group'", "'name'", "'password'", "'user'"]:
                print("Something's wrong with a user .json file...")
                print("Fixing the issue...")
                shutil.rmtree("../users")
                os.mkdir("../users")
                print("The issue should be fixed, try booting Aurora again.")

            else:
                print("The reason for the error can't be found... Instead:")
                print("Create an issue on the GitHub page or check the wiki on GitHub.")
elif recoverMode == "other":
    print("Since your problem doesn't match any of the provided options:")
    print("Create an issue on the GitHub page or check the wiki on GitHub.")