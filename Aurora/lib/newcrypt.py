#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

def onLoad():
    pass

# Do not touch the following variable definitions
encodings = {
    '0': ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
    '1': ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"],
    '2': ["k", "l", "m", "n", "o", "p", "q", "r", "s", "t"],
    '3': ["u", "v", "w", "x", "y", "z", ",", ".", ";", ":"],
    '4': ["!", "@", "#", "$", "%", "^", "&", "*", "(", ")"],
    '5': ["`", "~", "-", "_", "=", "+", "[", "]", "{", "}"],
    '6': ["|", "<", ">", "/", "?"],
    '7': ['"', "\\", " "]
}
def encrypt(string1):
    encstrlist = []
    for i in string1.lower():
        for encoding in encodings:
            if i in encodings[encoding]:
                encstrlist.append(encoding + str(encodings[encoding].index(i)))
                break
    return "".join(encstrlist)

def decrypt(string1):
    decstrlist = []
    for i in xrange(0, len(string1), 2):
        listin, indx = string1[i:i+2]
        decstrlist.append(encodings[listin][int(indx)])
    return "".join(decstrlist)